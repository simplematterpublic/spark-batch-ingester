import sys
import json
import datetime

from ingestion import ingestion


def compute_partition_for_hscode(filename):
    """Compute partition based on file name."""
    dt = datetime.datetime.strptime(filename, 'hscode_shipment_%Y%m%d.csv')

    return ['-year', str(dt.year), '-month', '{:02d}'.format(dt.month)]


if __name__ == '__main__':
    if len(sys.argv) == 1:
        raise Exception("configuration file path is expected as argument")
    elif len(sys.argv) == 2:
        config_path = sys.argv[1]
    else:
        raise Exception("too many arguments passed")

    print('args received:', sys.argv)

    with open(config_path, 'r') as config_file:
        config = json.load(config_file)

    print('config:', config)

    name = config['name']

    for item in config['items_for_ingestion']:
        source_dir = item['source']
        target_dir = item['target']
        file_type = item['type']
        filename_pattern = item['pattern']
        spark_job_conf = item['spark_job_conf']
        schema = item['schema'] if 'schema' in item else ''
        checkpoint_path = item['checkpoint_path']
        ingestion.run_ingestion(name,
                                source_dir,
                                target_dir,
                                file_type,
                                filename_pattern,
                                spark_job_conf,
                                schema,
                                checkpoint_path,
                                compute_partition_for_hscode)
