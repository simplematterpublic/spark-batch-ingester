import os
import argparse
import importlib
import pandas as pd
from ingestion import ingestion


def init_args():
    """Initialize argument list for the CLI."""
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parent_parser = argparse.ArgumentParser(add_help=False)

    parent_parser.add_argument('--name',
                               required=True,
                               help="name of the spark job")
    parent_parser.add_argument('--source',
                               required=True,
                               help="hdfs path to source directory")
    parent_parser.add_argument('--target',
                               required=True,
                               help="hdfs path to target directory")
    parent_parser.add_argument('--checkpoint',
                               required=True,
                               help="path to checkpoint file")
    parent_parser.add_argument('-p', '--partition_level',
                               choices=['', 'y', 'ym', 'ymw', 'ymwd', 'ymwdh'],
                               default='',
                               help='partition level to be used by partitioner')
    parent_parser.add_argument('-c', '--custom_partitioner',
                               help='local path to custom module '
                                    'with partitioner implementation')
    parent_parser.add_argument('-m', '--master',
                               choices=['yarn', 'local'],
                               default='yarn',
                               help='Run spark job on yarn or in local mode')
    parent_parser.add_argument('--username',
                               default=os.getenv('MAPR_CONTAINER_USER'),
                               help="username for hive")
    parent_parser.add_argument('--password',
                               default=os.getenv('MAPR_CONTAINER_PASSWORD'),
                               help="password for hive")
    parent_parser.add_argument('--host',
                               default=os.getenv('HIVE_HOST'),
                               help="host for hive")
    parent_parser.add_argument('--port',
                               default=os.getenv('HIVE_PORT'),
                               help="port for hive")
    parent_parser.add_argument('--database',
                               required=True,
                               help="hive database name")
    parent_parser.add_argument('--table',
                               required=True,
                               help="hive table name")

    subparsers = parser.add_subparsers(
        dest='file_type',
        help='type of the files to be ingested')

    # csv
    parser_csv = subparsers.add_parser(
        'csv', parents=[parent_parser], help='ingest csv files',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_csv.add_argument('--schema',
                            required=True,
                            help="path to schema definintion file")
    parser_csv.add_argument('--sep',
                            default='"|"',
                            help="csv separator character")
    parser_csv.add_argument('--header',
                            default=True,
                            help='csv header')
    parser_csv.add_argument('--ltrim',
                            default=True,
                            help="ignore leading whitespace in csv cells")
    parser_csv.add_argument('--rtrim',
                            default=True,
                            help="ignore trailing whitespace in csv cells")

    # json
    parser_json = subparsers.add_parser(
        'json', parents=[parent_parser], help='ingest json files',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_json.add_argument('--multiline',
                             default=False,
                             help="allow multiline json items")
    parser_json.add_argument('--primitives_as_string',
                             default=False,
                             type=bool,
                             help='convert primitive types to string')

    # xml (has no options)
    parser_xml = subparsers.add_parser(
        'xml', parents=[parent_parser], help='ingest xml files',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    args = parser.parse_args()

    print('Command line args:')
    for arg in vars(args):
        print('----', arg, getattr(args, arg))

    return args


def compute_week_of_month(dt):
    """Compute week-of-month.

    Computation of week-of-month depends on calendar defaults.
    This implementation assumes the following defaults:
    * start of the week is Monday
    * if the week transitions between two months, then the last few days
        are considered as 5th or 6th week, then the remaining days are
        considered as 1st week because they are in the beginning of the month.
    """
    ts = pd.Timestamp(dt)
    ts_start = pd.Timestamp(year=ts.year, month=ts.month, day=1)

    first_week_of_month = ts_start.week

    # When first week of month is still stuck in last years count
    # and the current week count is reset, then we need to add last year's
    # amount of weeks (first_week_of_month, which is 52 or more) to
    # compute the difference correctly
    if first_week_of_month >= 47 and ts.week <= 6:
        current_week = ts.week + (first_week_of_month
                                  if first_week_of_month > 52 else 52)
    else:
        current_week = ts.week

    # Here we compute the difference between current week and
    # the first week of the month.
    week_of_month = current_week - first_week_of_month + 1

    # for debugging
    # print(ts.date(), first_week_of_month, ts.week, current_week,
    #       week_of_month)

    return week_of_month


def compute_partition(partition_level, filename=''):
    """Compute partition based on current time.

    Args:
        filename (str): required in case of custom logic of partition
            computation, not used here (useful for historical import)
        partition (str): a sequence of characters in the following
            order: 'ymwdh'. Actual partitioning level is controlled
            by the starting subsequence of the given string, e.g.
            for monthly partitioning the string would be 'ym',
            for daily partitioning - 'ymwd'.
    Returns:
        partition_args (list(str)): list of strings directly usable as
            partitioning arguments for spark job."""
    print("executing default compute_partition")
    now = pd.Timestamp.now()

    if partition_level == "y":
        parts = ["-year", str(now.year)]
    elif (partition_level == "ym"):
        parts = ["-year", str(now.year),
                 "-month", '{:02d}'.format(now.month)]
    elif partition_level == "ymw":
        parts = ["-year", str(now.year),
                 "-month", '{:02d}'.format(now.month),
                 "-week", str(compute_week_of_month(now))]
    elif partition_level == "ymwd":
        parts = ["-year", str(now.year),
                 "-month", '{:02d}'.format(now.month),
                 "-week", str(compute_week_of_month(now)),
                 "-day", '{:02d}'.format(now.day)]
    elif partition_level == "ymwdh":
        parts = ["-year", str(now.year),
                 "-month", '{:02d}'.format(now.month),
                 "-week", str(compute_week_of_month(now)),
                 "-day", '{:02d}'.format(now.day),
                 "-hour", '{:02d}'.format(now.hour)]
    else:
        raise Exception("Unknown partition_level %s" % partition_level)

    return parts


def main():
    """Entry point of the ingestion."""
    # parse arguments
    args = init_args()

    # perform the ingestion
    ingestion.run_ingestion(args)


if __name__ == '__main__':
    main()

# +++ separate custom partitioning function
#     and load via config dynamically
#
# +++ by default make current time based
#     partitioning upto certain level
#
# +++ perform checks against paths
#
# +++ create hive database, table and refresh hive table
#
# +++ move config items into args list
#
# +++ remove pattern matching of file names
#
# +++ use maprfs for config, checkpoints, etc.
#
# +++ update spark-job to support config options as arguments

# +++ after successful ingestion use or create a database
#
# +++ check if target table exists:
#     if yes, then refresh table, else create from the lately ingested data
#
# +++ add hive-related arguments
#
# +++ rebuild docker image
#
# +++ test all hive related above
#
# +++ if produced partitions does not match level, raise exception
#
# +++ check if the data to ingest is incremental or overwrite
#     (overwrite is useful for lookup tables and small metadata)

# +++ test for no partitioning:
#       1. python args
#       2. python checks
#       3. spark-job call
#       4. spark-job execution
#
# https://community.hortonworks.com/articles/101645/adding-new-columns-to-an-already-partitioned-hive.html
# ALTER TABLE CHANGE COLUMN with CASCADE command changes the columns of a table's metadata, and cascades the same change to all the partition metadata
