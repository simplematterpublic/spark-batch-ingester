cp ingest-all.py temp/ && \
cp init-hive.py temp/ && \
cp ingest-metadata.py temp/ && \
cp README.md temp/ && \
cp -R ingestion temp/ && \
cp partitioner_ym.py temp/ && \
cp partitioner_ymwd.py temp/ && \
cp ../spark-batch-ingester/target/spark-batch-ingester-1.0-SNAPSHOT.jar temp/spark-job/ && \
tar -czvf maprcore-ingest.tar temp/
