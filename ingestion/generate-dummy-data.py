import os
import random
import argparse
import pandas as pd


def next_int():
    return random.randint(0, 10000)


def next_string(size=8):
    return ''.join([random.choice('abcdefghijklmnopqrstuvwxyz')
                    for i in range(size)])


def next_bool():
    return bool(next_int() % 2)


def makedirs(path):
    if not os.path.exists(path):
        os.makedirs(path)


def generate_df(extra_column=False):
    columns = ['col1', 'col2', 'col3', 'col4', 'col5'] + \
        (['col6'] if extra_column else [])

    if extra_column:
        index = pd.date_range(start='2017-01-01',
                              end='2018-01-01', closed='left', freq='1T')
    else:
        index = pd.date_range(start='2015-01-01',
                              end='2017-01-01', closed='left', freq='1T')

    row_count = len(index)

    col1 = [next_int() for _ in range(row_count)]
    col2 = [next_string(20) for _ in range(row_count)]
    col3 = [timestamp.strftime('%Y%m') for timestamp in index]
    col4 = [next_bool() for _ in range(row_count)]
    col5 = index.to_series()
    if extra_column:
        col6 = [next_string(4) for _ in range(row_count)]

    data_lists = [col1, col2, col3, col4, col5] + \
        ([col6] if extra_column else [])

    df = pd.DataFrame({col: data for col, data in zip(
        columns, data_lists)}, columns=columns)

    return df


def init_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parent_parser = argparse.ArgumentParser(add_help=False)

    parent_parser.add_argument('--target',
                               required=True,
                               help="Path to directory where the files should be generated")

    subparsers = parser.add_subparsers(
        dest='file_type',
        help='type of the files to be generated')

    # csv
    parser_csv = subparsers.add_parser(
        'csv', parents=[parent_parser], help='generate csv files',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_csv.add_argument('--sep',
                            default='|',
                            help="csv separator character")
    parser_csv.add_argument('--header',
                            default=True,
                            help='csv header')

    # json
    parser_json = subparsers.add_parser(
        'json', parents=[parent_parser], help='generate json files',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    args = parser.parse_args()

    print('Command line args:')
    for arg in vars(args):
        print('----', arg, getattr(args, arg))

    return args


def generate_dummy_data(args):
    if args.file_type not in ['csv', 'json']:
        raise Exception("File type {} not supported".format(args.file_type))

    df = generate_df()
    gr = df.groupby(pd.Grouper('col3'), as_index=False).groups
    daily_dfs = {key: df.loc[index] for key, index in gr.items()}

    makedirs(args.target)

    if args.file_type == 'csv':
        for key, df in daily_dfs.items():
            path = os.path.join(args.target, 'test_data_{}.csv'.format(key))
            df.reset_index(drop=True).to_csv(path, sep=args.sep, index=False)
    elif args.file_type == 'json':
        for key, df in daily_dfs.items():
            path = os.path.join(args.target, 'test_data_{}.json'.format(key))
            df.reset_index(drop=True).to_json(path,
                                              orient='records',
                                              date_format='iso',
                                              force_ascii=False,
                                              lines=True)
    else:
        pass


def main():
    # parse arguments
    args = init_args()

    generate_dummy_data(args)


if __name__ == '__main__':
    main()
