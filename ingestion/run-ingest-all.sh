# local
python ingest-all.py csv \
    --sep ";" \
    --name test_job \
    --source /cygwin64/home/rtatishv/data/testing/csv/staging \
    --target /cygwin64/home/rtatishv/data/testing/csv/master \
    --schema schema-test-csv.json \
    --checkpoint checkpoints/test/checkpoint_csv.txt \
    --partition_level ym \
    --custom_partitioner test_partitioner_csv


# run on cluster from inside container
python ingest-all.py csv \
    --sep ";" \
    --name test_job \
    --source maprfs:///playground/staging/testing/csv \
    --target maprfs:///playground/master/testing/csv \
    --schema /mapr/dpdhl_datalake/playground/schemas/schema-test-csv.json \
    --checkpoint /mapr/dpdhl_datalake/checkpoints/testing/checkpoint_csv.txt \
    --partition_level ym \
    --custom_partitioner test_partitioner_csv \
    --database test_ingestion1 \
    --table test_table1
    
    
# run on cluster from host as exec into a container
docker exec -it maprcore-ingest /opt/miniconda/bin/python /app/ingest-all.py csv \
    --sep ";" \
    --name test_job \
    --source maprfs:///playground/staging/testing/csv \
    --target maprfs:///playground/master/testing/csv \
    --schema /mapr/dpdhl_datalake/playground/schemas/schema-test-csv.json \
    --checkpoint /mapr/dpdhl_datalake/checkpoints/testing/checkpoint_csv.txt \
    --partition_level ym \
    --custom_partitioner test_partitioner_csv \
    --database test_ingestion1 \
    --table test_table1

# run init-hive
docker exec -it maprcore-ingest5 /opt/miniconda/bin/python /app/init-hive.py csv \
    --target maprfs:///playground/master/testing/csv \
    --schema /mapr/dpdhl_datalake/playground/schemas/schema-test-csv.json \
    --partition_level ym \
    --database test_ingestion1 \
    --table test_table1


# run on cluster from host as exec into a container
# ingest metadata in overwrite mode
docker exec -it maprcore-ingest7 /opt/miniconda/bin/python /app/ingest-metadata.py csv \
    --sep ";" \
    --name test_job \
    --source maprfs:///playground/staging/testing/metadata \
    --target maprfs:///playground/master/testing/metadata \
    --schema /mapr/dpdhl_datalake/playground/schemas/schema-metadata-csv.json \
    --database test_ingestion1 \
    --table test_metadata
    
# no partitioning  and empty custom partitioner
python ingest-all.py csv \
    --sep ";" \
    --name test_job \
    --source maprfs:///playground/staging/testing/csv \
    --target maprfs:///playground/master/testing/nopart_csv \
    --schema /mapr/dpdhl_datalake/playground/schemas/schema-test-csv.json \
    --checkpoint /mapr/dpdhl_datalake/checkpoints/testing/checkpoint_nopart_csv.txt \
    --custom_partitioner empty_partitioner_csv \
    --database test_ingestion1 \
    --table test_table_no_part
