import os
import argparse
import importlib
import pandas as pd
from ingestion import ingestion


def init_args():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parent_parser = argparse.ArgumentParser(add_help=False)

    parent_parser.add_argument('--target',
                               required=True,
                               help="hdfs path to target directory")
    parent_parser.add_argument('-p', '--partition_level',
                               required=True,
                               choices=['y', 'ym', 'ymw', 'ymwd', 'ymwdh'],
                               help='partition level to be used by partitioner')
    parent_parser.add_argument('--username',
                               default=os.getenv('MAPR_CONTAINER_USER'),
                               help="username for hive")
    parent_parser.add_argument('--password',
                               default=os.getenv('MAPR_CONTAINER_PASSWORD'),
                               help="password for hive")
    parent_parser.add_argument('--host',
                               default='czchols4221',
                               help="host for hive")
    parent_parser.add_argument('--port',
                               default='8047',
                               help="port for hive")
    parent_parser.add_argument('--database',
                               required=True,
                               help="hive database name")
    parent_parser.add_argument('--table',
                               required=True,
                               help="hive table name")

    subparsers = parser.add_subparsers(
        dest='file_type',
        help='type of the files to be ingested')

    # csv
    parser_csv = subparsers.add_parser(
        'csv', parents=[parent_parser], help='ingest csv files',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)
    parser_csv.add_argument('--schema',
                            required=True,
                            help="path to schema definintion file")

    args = parser.parse_args()

    print('Command line args:')
    for arg in vars(args):
        print('----', arg, getattr(args, arg))

    return args


def main():
    """Entry point of the ingestion."""
    # parse arguments
    args = init_args()

    # perform the ingestion
    ingestion.init_hive(args)


if __name__ == '__main__':
    main()
