java -jar -Dconfig=xml.conf target/spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar \
    -master local \
    -source ../../../data/testing/xml/source/sample.xml \
    -target ../data/playground/master/xml/sample \
    -type xml \
