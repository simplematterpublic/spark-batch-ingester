/opt/mapr/spark/spark-2.2.1/bin/spark-submit \
    --name "data-ingestion-opms" \
    --master yarn \
    --deploy-mode cluster \
    --files customconfig.properties#customconfig.properties \
    --driver-java-options -Dconfig=customconfig.properties \
    data-ingestion-opms-1.0-SNAPSHOT-jar-with-dependencies.jar \
        maprfs:///expressdelivery/staging/OPMS/evc_deststn_2017/OPMS_BE_evc_2017_nov_stn_0_1.txt \
        maprfs:///expressdelivery/master/OPMS/evc_deststn
