java -jar -Dconfig=json.conf spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar \
    -master local \
    -source ../../../data/testing/json/01/test_data_20150101.json \
    -target ../data/playground/master/json/01 \
    -type json \
