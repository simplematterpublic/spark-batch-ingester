import datetime


def compute_partition(partition_level='', filename=''):
    """Compute partition based on file name."""
    print("executing hscode.compute_partition")
    dt = datetime.datetime.strptime(filename, 'test_data_%Y%m%d.csv')

    return ['-year', str(dt.year), '-month', '{:02d}'.format(dt.month),
            '-week', '0', '-day', '{:02d}'.format(dt.day)]
