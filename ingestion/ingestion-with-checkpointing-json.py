import os
import sys
import glob
import json
import uuid
import datetime
import subprocess

YARN = True

MONTH_DICT = {
    '_jan_': '01',
    '_feb_': '02',
    '_mar_': '03',
    '_apr_': '04',
    '_may_': '05',
    '_jun_': '06',
    '_jul_': '07',
    '_aug_': '08',
    '_sep_': '09',
    '_oct_': '10',
    '_nov_': '11',
    '_dec_': '12'
}


def execute_hadoop_ls_with_grep(source_dir, filename_pattern):
    print('executing hadoop ls with grep')
    # probably -R not needed any more
    command = ['hadoop', 'fs', '-ls', source_dir]
    hadoop_ls_proc = subprocess.Popen(command, stdout=subprocess.PIPE)
    grep_proc = subprocess.Popen(['grep', filename_pattern],
                                 stdin=hadoop_ls_proc.stdout,
                                 stdout=subprocess.PIPE)
    output = grep_proc.stdout

    files = get_file_list_from_hadoop_output(output)

    return files


def execute_posix_ls_with_grep(source_dir, filename_pattern):
    print('executing posix ls with grep')

    # dirs = [path.replace('\\', '/') for path in sorted(
    #             glob.glob(source_dir.rstrip('/') + '/*'))]
    # # this should work fine
    files = [
        path.replace('\\', '/')
        for path in sorted(glob.glob(
            source_dir.rstrip('/') + '/*' + filename_pattern.replace('$', '')))
    ]

    return files


def get_file_list_from_hadoop_output(hadoop_output):
    output_lines = [line.strip().decode() for line in hadoop_output
                    if len(line.strip())]
    output_matrix = [line.split(" ") for line in output_lines]
    file_list = [row[-1].strip() for row in output_matrix if len(row) > 6]

    return file_list


def get_unix_timestamp_now():
    """Return current datetime as unix timestamp in milliseconds."""
    ts = get_unix_timestamp(None)

    return ts


def get_unix_timestamp(dt):
    """Return datetime as unix timestamp in milliseconds."""
    dt = datetime.datetime.utcnow() if dt is None else dt
    # failed on python 2.7
    # ts = int(dt.timestamp() * 1000)
    delta = dt - datetime.datetime(1970, 1, 1)
    ts = int(delta.total_seconds() * 1000)

    return ts


def generate_uuid_string(with_dashes=True):
    """Generate random uuid."""
    id = uuid.uuid4()
    id_string = str(id) if with_dashes else id.hex

    return id_string


def generate_unique_name(dt=None):
    """Generate unique name consisting of [UUID]_[TIMESTAMP].

    UUID is formatted as 4a46ed0d-0f55-4035-a03c-0799636e09d3,
    TIMESTAMP is a unix timestamp in milliseconds.
    """
    uuid_part = generate_uuid_string()
    timestamp_part = str(get_unix_timestamp(dt))

    return '_'.join([uuid_part, timestamp_part])


def extract_partitions(source_file):

    dir_path = os.path.dirname(source_file)
    filename = os.path.basename(source_file)

    year = os.path.basename(dir_path).split('_')[-1]
    month = None
    for key in MONTH_DICT.keys():
        if key in filename:
            month = MONTH_DICT[key]
            break

    try:
        int(year)
    except:
        raise Exception('year can not be extracted from source file')

    if month is None:
        raise Exception('month can not be extracted from source file')

    return year, month


def execute_job(name, source_file, target_path, file_type, hdfs_mode):
    print('source file:', source_file)
    print('target path:', target_path)
    print('hdfs_mode:', hdfs_mode)

    # year, month = extract_partitions(source_file)
    # year, month = "2016", "05"
    if file_type == 'json':
        filename = os.path.basename(source_file)
        year, month = filename[10:14], filename[14:16]
        dt = datetime.datetime(year=int(year), month=int(month), day=1)
        target_unique_dir = target_path.rstrip(
            '/') + '/' + generate_unique_name(dt)

    if hdfs_mode:
        args = [
            '/opt/mapr/spark/spark-2.2.1/bin/spark-submit',
            '--name',
            'data-ingestion-temp' + '-' + name,
            '--master',
            'yarn' if YARN else 'local',
            '--deploy-mode',
            'cluster' if YARN else 'client',
            '--files' if YARN else '',
            'json.conf#json.conf' if YARN else '',
            '--driver-java-options',
            '-Dconfig=json.conf',
            'spark-job/spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar'
        ] + [
            '-master', 'cluster',
            '-source', source_file,
            '-target', target_unique_dir,
            '-type', file_type
            # '-schema', 'schema_evc_deststn.json',
            # '-year', year,
            # '-month', month
        ]
    else:
        args = [
            'java',
            '-jar',
            '-Dconfig=customconfig.properties',
            'spark-job/spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar'
        ] + [
            '-master', 'local',
            '-source', source_file,
            '-target', target_path,
            '-type', file_type,
            # '-schema', 'schema.json',
            '-year', year,
            '-month', month
        ]

    print('executing command:', ' '.join(args))
    proc = subprocess.Popen(args,
                            stderr=subprocess.PIPE,
                            universal_newlines=True)
    output = proc.communicate()
    with open('stderr/' + os.path.basename(source_file), 'w') as proc_output:
        proc_output.write(output[1])

    return proc.returncode


def checkpoint_diff(checkpoint_path, file_list):
    # read checkpoint file and diff names
    with open(checkpoint_path, 'r') as fh:
        checkpointed_files = [line.strip() for line in fh]
        print('checkpointed_files:')
        print(checkpointed_files)
    # emit pseudo-command for each non-processed file
    unprocessed_files = [file for file in file_list
                         if file not in checkpointed_files]
    print('unprocessed_files:')
    print(unprocessed_files)

    return unprocessed_files


def produce_target_path(source_file, source_dir, target_dir):
    source_dir_path = os.path.dirname(source_file)
    subdir_name = source_dir_path.replace(source_dir, '').strip('/')
    target_dir_path = target_dir.rstrip('/') + '/' + subdir_name

    return target_dir_path


def run_ingestion(name,
                  source_dir,
                  target_dir,
                  file_type,
                  filename_pattern,
                  checkpoint_path):
    hdfs_mode = (source_dir.startswith('maprfs://')
                 or source_dir.startswith('hdfs://'))
    file_list = (
        execute_hadoop_ls_with_grep(source_dir, filename_pattern)
        if hdfs_mode
        else execute_posix_ls_with_grep(source_dir, filename_pattern)
    )

    unprocessed_files = checkpoint_diff(checkpoint_path, file_list)
    # TODO: remove after testing
    # unprocessed_files = unprocessed_files[:16:4]

    target_path_list = [
        produce_target_path(source_file, source_dir, target_dir)
        for source_file in unprocessed_files
    ]

    for file, target_path in zip(unprocessed_files, target_path_list):
        return_code = execute_job(
            name, file, target_path, file_type, hdfs_mode)
        # if successful, add name to the checkpoint
        if return_code == 0:
            with open(checkpoint_path, 'a') as updated_checkpoint_file:
                updated_checkpoint_file.readline
                updated_checkpoint_file.write(file)
                updated_checkpoint_file.write('\n')

            print('Processed', file)
        else:
            print('Failed', file)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        raise Exception("configuration file path is expected as argument")
    elif len(sys.argv) == 2:
        config_path = sys.argv[1]
    else:
        raise Exception("too many arguments passed")

    print('args received:', sys.argv)

    with open(config_path, 'r') as config_file:
        config = json.load(config_file)

    print('config:', config)

    name = config['name']

    for item in config['items_for_ingestion']:
        source_dir = item['source']
        target_dir = item['target']
        file_type = item['type']
        filename_pattern = item['pattern']
        checkpoint_path = item['checkpoint_path']
        run_ingestion(name,
                      source_dir,
                      target_dir,
                      file_type,
                      filename_pattern,
                      checkpoint_path)
