# Batch Ingestion Tool

### Introduction

The batch ingestion tool is a spark batch job accompanied with a python project managing it, all packaged as a docker image. The goal of the tool is to ingest the data available on the Data Lake in the staging area of the distributed file system to the master data and make it available for querying through Hive.

### Usage

Ingestion tool is available as a running docker container with name `maprcore-ingest` on the node `czchols4342`. To setup data ingestion for a new data source one need to have the data available on the staging area (directory on hdfs) and prepare the docker execution command with the respective argument list. The entire execution can be also put on cron if necessary.

below we demonstrate an example of such a command:

```
docker exec -it maprcore-ingest /opt/miniconda/bin/python /app/ingest-all.py csv \
    --sep ";" \
    --name test_job \
    --source maprfs:///playground/staging/testing/csv \
    --target maprfs:///playground/master/testing/csv \
    --schema /mapr/dpdhl_datalake/playground/schemas/schema-test-csv.json \
    --checkpoint /mapr/dpdhl_datalake/checkpoints/testing/csv_ym.txt \
    --partition_level ym \
    --custom_partitioner partitioner_ym \
    --database test_ingestion1 \
    --table table_ym
```

Here we launch `/app/ingest-all.py` in the running container with interactive console `-it` and provide long list of arguments. For the full list of arguments see Reference.

Docker eco-system enables us to launch another container of the same image if necessary. The reason can be testing or evaluation of new version, historical ingestion where some environment variables have to be overriden, etc. Below we provide an example how to launch another container of the maprcore-ingest docker image:

```
docker run -dti \
-e HOST_IP=machine123 \
-e MAPR_CLUSTER=dpdhl_datalake \
-e MAPR_CLDB_HOSTS=machine111:54321,machine112:54321,machine113:54321 \
-e MAPR_CONTAINER_USER=someuser \
-e MAPR_CONTAINER_PASSWORD=somepassword \
-e MAPR_CONTAINER_UID=12345 \
-e MAPR_CONTAINER_GID=12345 \
-e MAPR_CONTAINER_GROUP=someuser \
-e MAPR_TICKETFILE_LOCATION=/tmp/mapr_ticket -v /tmp/maprticket_12345:/tmp/mapr_ticket:ro \
-e MAPR_MOUNT_PATH=/mapr \
-e MAPR_HS_HOST=machine111 \
-e HIVE_PORT=8444 \
--cap-add SYS_ADMIN --cap-add SYS_RESOURCE --device /dev/fuse --security-opt apparmor:unconfigured \
--name maprcore-ingest datalake/maprcore-ingest:1.0
```

The environment variables are either set in docker image or provided on docker run as demonstrated above.


At the moment there are 3 scripts available to the end-user:
  * `/app/ingest-all.py`
  * `/app/ingest-metadata.py`
  * `/app/init-hive.py`

**ingest-all** is responsible for regular data ingestion process. 

For online data ingestion the arguments should be maintained unchanged across the runs: checkpoint, partition_level, table, etc. A custom partitioner is not needed for online data ingestion as the correct partition is deduced in runtime based on current date and time rather than from the timestamp found in file or directory names. 

For historical data ingestion custom_partitioner is required as the different parts of data should be time-wise distributed into different partitions. Also it is important to have both checkpoint file and target path empty before the start of historical data ingestion. 

**ingest-metadata** is responsible for ingestion of application metadata. Under application metadata we mean static tables containing enum values, descriptions, lookup tables, etc. which change very rarely and their content is not time-aware. ingest-metadata uses the spark-job in overwrite mode, i.e. it ingests the new version of the full data in place of the old. This is acceptable as such data is not changing frequently and also they do not have regular growth trend in size.

**init-hive** is responsible for preparing hive database and table upfront without performing any data ingestion.

### Reference

In this reference you will find the list of arguments with respective descriptions available for each command option.

#### Ingest All

`ingest-all.py` accepts following commands: `csv`, `json` and `xml`. For all of the commands there are both commonly applicable and command-specific arguments. All of the arguments are listed below.

Commonly applicable arguments:

| argument     | required | description |
|:-------------|:---------|:------------|
| `--name` | True | name suffix of the spark-job. E.g. `--name test_job` will launch a job with name `data-ingestion-test_job` |
| `--source` | True | HDFS path to the data source on staging area |
| `--target` | True | HDFS path to the target directory on master area |
| `--checkpoint` | True | HDFS path to the checkpoint file. Checkpoint file is used to account for already processed files. If checkpoint file does not exist it is created |
| `--database` | True | HIVE database name. If the database with provided name does not exit it will be created. The database naming should follow the customer name and user name naming convention. See the naming conventions for more details |
| `--table` | True | HIVE table name. If the table with provided name does not exist it will be created. Note: the script does not allow to create new table for already used target path and it also does not allow to use different target path for existing table, i.e. once created, table should be located at the same path on master area |
| `-p`, `--partition_level` | False | Partition level used for hive tables. One of the values `y`, `ym`, `ymw`, `ymwd`, `ymwdh`, each letter denoting year, month, week, day and hour respectively. If not provided no partitioning is used by default |
| `-c`, `--custom_partitioner` | False | Name of the module that contains a custom partitioner function. The goal of having such a function is to extract partitioning information from file names. This is useful for ingestion of historical files into desired partitioning scheme. The function need to match the expected signature and returning data structure. The details can be found in 'Custom Partitioner'  |
| `-m`, `--master` | False | Run spark job on yarn or in local mode. Possible values: `yarn` or `local`. By default `yarn` |
| `--username` | False | Username to use for Hive (MAPR authentication). If not provided, default value is taken from `MAPR_CONTAINER_USER` environment variable  |
| `--password` | False | Password of the username for Hive (MAPR authentication). If not provided, default value is taken from `MAPR_CONTAINER_PASSWORD` environment variable  |
| `--host` | False | Host for Hive. If not provided, default value is taken from `HIVE_HOST` environment variable  |
| `--port` | False | Port number for Hive. If not provided, default value is taken from `HIVE_PORT` environment variable  |

csv-specific arguments:

| argument     | required | description |
|:-------------|:---------|:------------|
| `--sep` | False | CSV separator character. By default: '&#124;' |
| `--header` | False | CSV header (`true` or `false`). By default: `true` |
| `--schema` | True | Path to schema JSON. For the content details of this file please refer to 'Schema Definition Guide'. |
| `--ltrim` | False | Ignore leading whitespace in CSV cells (`true` or `false`). By default: `true` |
| `--rtrim` | False | Ignore trailing whitespace in CSV cells (`true` or `false`). By default: `true` |

json-specific arguments:

| argument     | required | description |
|:-------------|:---------|:------------|
| `--multiline` | False | Allow multiline JSON items. By default: `false` |
| `--primitives_as_string` | False | Convert primitive types to string (`true` or `false`). By default: `false` |

xml-specific arguments: 

No additional arguments available for XML.


#### Ingest metadata

At the moment `ingest-metadata.py` script accepts command `csv` only and respective arguments:

(Note: partitioning and checkpointing is not relevant in this case)

Commonly applicable arguments:

| argument     | required | description |
|:-------------|:---------|:------------|
| `--name` | True | name suffix of the spark-job. E.g. `--name test_job` will launch a job with name `data-ingestion-test_job` |
| `--source` | True | HDFS path to the data source on staging area |
| `--target` | True | HDFS path to the target directory on master area |
| `--database` | True | HIVE database name. If the database with provided name does not exit it will be created. The database naming should follow the customer name and user name naming convention. See the naming conventions for more details |
| `--table` | True | HIVE table name. If the table with provided name does not exist it will be created. Note: the script does not allow to create new table for already used target path and it also does not allow to use different target path for existing table, i.e. once created, table should be located at the same path on master area |
| `-m`, `--master` | False | Run spark job on yarn or in local mode. Possible values: `yarn` or `local`. By default `yarn` |
| `--username` | False | Username to use for Hive (MAPR authentication). If not provided, default value is taken from `MAPR_CONTAINER_USER` environment variable |
| `--password` | False | Password of the username for Hive (MAPR authentication). If not provided, default value is taken from `MAPR_CONTAINER_PASSWORD` environment variable |
| `--host` | False | Host for Hive. If not provided, default value is taken from `HIVE_HOST` environment variable |
| `--port` | False | Port number for Hive. If not provided, default value is taken from `HIVE_PORT` environment variable |

csv-specific arguments:

| argument     | required | description |
|:-------------|:---------|:------------|
| `--sep` | False | CSV separator character. By default: '&#124;' |
| `--header` | False | CSV header (`true` or `false`). By default: `true` |
| `--schema` | True | Path to schema JSON. For the content details of this file please refer to 'Schema Definition Guide'. |
| `--ltrim` | False | Ignore leading whitespace in CSV cells (`true` or `false`). By default: `true` |
| `--rtrim` | False | Ignore trailing whitespace in CSV cells (`true` or `false`). By default: `true` |



#### Initialize Hive Table
 
At the moment `init-hive.py` script accepts command `csv` only and respective arguments: 

Commonly applicable arguments:

| argument     | required | description |
|:-------------|:---------|:------------|
| `--target` | True | HDFS path to the target directory on master area |
| `-p`, `--partition_level` | False | Partition level used for hive tables. One of the values `y`, `ym`, `ymw`, `ymwd`, `ymwdh`, each letter denoting year, month, week, day and hour respectively. If not provided no partitioning is used by default |
| `--username` | False | Username to use for Hive (MAPR authentication). If not provided, default value is taken from `MAPR_CONTAINER_USER` environment variable |
| `--password` | False | Password of the username for Hive (MAPR authentication). If not provided, default value is taken from `MAPR_CONTAINER_PASSWORD` environment variable |
| `--host` | False | Host for Hive. If not provided, default value is taken from `HIVE_HOST` environment variable |
| `--port` | False | Port number for Hive. If not provided, default value is taken from `HIVE_PORT` environment variable |
| `--database` | True | HIVE database name. If the database with provided name does not exit it will be created. The database naming should follow the customer name and user name naming convention. See the naming conventions for more details |
| `--table` | True | HIVE table name. If the table with provided name does not exist it will be created. Note: the script does not allow to create new table for already used target path and it also does not allow to use different target path for existing table, i.e. once created, table should be located at the same path on master area |

csv-specific arguments:

| argument     | required | description |
|:-------------|:---------|:------------|
| `--schema` | True | Path to schema JSON. For the content details of this file please refer to 'Schema Definition Guide'. |


### Schema Definition Guide

In this subsection we show how to define schema for the incoming csv files. As we know csv files are schemaless and untyped. During the ingestion csv data is converted into "richer" data format where the data types are considered. For this we define a schema into a json file where each item of the "schema" array is a data structure consisting of "name" (required), "type" (required), "value" (optional) and "pattern" (optional). Below there is an example of a schema:

```json
{
  "schema": [
    {"name": "col1", "type": "long"},
    {"name": "col2", "type": "string"},
    {"name": "col3", "type": "string"},
    {"name": "col4", "type": "boolean"},
    {"name": "col5", "type": "timestamp", "pattern": "yyyy-MM-dd HH:mm:ss"}
  ]
}
```

Supported types:

  * boolean
  * string
  * short
  * integer
  * long
  * float
  * double
  * decimal
    * expected to have pattern field in form of `"(PRECISION,SCALE)"`, e.g. `"(20,4)"`
  * date
    * expected to have pattern field in form of datetime formatting pattern, e.g. `"yyyy-MM-dd"`
  * timestamp
    * expected to have pattern field in form of datetime formatting pattern, e.g. `"yyyy-MM-dd HH:mm:ss"`


### Custom Partitioner

Custom partitioner allows end user to provide partitioner that will deduce correct partition arguments based on the file name to be processed. This capability is useful in case of historical data import.

Custom partitioner module should contain a function with following signature:

```python
compute_partition(partition_level='', filename='')
```

and return the list of strings, where the items should be ordered as follows:

```python
['-year', YEAR_DERIVED_FROM_FILENAME, 
 '-month', MONTH_DERIVED_FROM_FILENAME, 
 '-week', WEEK_DERIVED_FROM_FILENAME, 
 '-day', DAY_DERIVED_FROM_FILENAME, 
 '-hour', HOUR_DERIVED_FROM_FILENAME]
```

And for shallower levels of partitioning respective argument names and values should be excluded. As an example you can see below a partitioner that partitions on 'ym' level:

```python
def compute_partition(partition_level='', filename=''):
    """Compute partition based on file name."""
    print("executing example.compute_partition")
    dt = datetime.datetime.strptime(filename, 'test_data_%Y%m%d.csv')

    return ['-year', str(dt.year), '-month', '{:02d}'.format(dt.month)]

```


### Some tips about deployment

To deploy the solution as a whole the source code needs to be packaged into a docker image. Currently this is achieved manually on the edge node itself (until we build CI/CD pipeline for the project).  


Given that all necessary files are structured under `data` subdirectory we use the following Dockerfile to describe the image:

```
FROM datalake/maprcore-miniconda:latest

ARG HIVE_HOST
ENV HIVE_HOST ${HIVE_HOST:-<DEFAULT_HIVE_HOST>}

ARG HIVE_PORT
ENV HIVE_PORT ${HIVE_PORT:-10000}

COPY ./data /app
```

and following command to build new image and tag it (Note: version update is maintained manually until we build CI/CD pipeline for the project):
```bash
docker build -t datalake/maprcore-ingest:1.0 . && docker tag datalake/maprcore-ingest:1.0 datalake/maprcore-ingest:latest
```

Based on this image at least one container will be up and running to serve ordinary ingestion scenarios as multiple docker-exec command can be issued against single container. Only if some special update is needed, one can provide new image and based on that new container which will serve special ingestion cases without interfering with the current one.

```
docker run -dti \
-e HOST_IP=machine123 \
-e MAPR_CLUSTER=dpdhl_datalake \
-e MAPR_CLDB_HOSTS=machine111:54321,machine112:54321,machine113:54321 \
-e MAPR_CONTAINER_USER=someuser \
-e MAPR_CONTAINER_PASSWORD=somepassword \
-e MAPR_CONTAINER_UID=12345 \
-e MAPR_CONTAINER_GID=12345 \
-e MAPR_CONTAINER_GROUP=someuser \
-e MAPR_TICKETFILE_LOCATION=/tmp/mapr_ticket -v /tmp/maprticket_12345:/tmp/mapr_ticket:ro \
-e MAPR_MOUNT_PATH=/mapr \
-e MAPR_HS_HOST=machine111 \
-e HIVE_PORT=8444 \
--cap-add SYS_ADMIN --cap-add SYS_RESOURCE --device /dev/fuse --security-opt apparmor:unconfigured \
--name maprcore-ingest-custom datalake/maprcore-ingest:1.1
```

Notice the `--name maprcore-ingest-custom datalake/maprcore-ingest:1.1` at the final line of the command that will create another container instance from the modified image (e.g. having different custom partitioner).
