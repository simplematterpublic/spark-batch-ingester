import json
import re
from pyhive import hive


class HiveConnector(object):
    """Hive connector class.

    HiveConnector is responsible to initialize and hold the connection
    while providing convenient methods that are wrappers for frequently
    used queries.
    """

    def __init__(self, host, port, username, password, auth):
        self.conn = hive.connect(host=host,
                                 port=port,
                                 username=username,
                                 password=password,
                                 auth=auth)
        self.cur = self.conn.cursor()

    def _check_name_alphanumeric(self, name):
        """Check if name contains only alphanumeric values.

        A helper function to check the names before DDL operations.
        """
        if not re.fullmatch('^\w+$', name):
            raise Exception("Name is not alphanumeric")

    def create_database_if_not_exists(self, database):
        """Create Hive database if not exists."""
        sql = ("CREATE DATABASE IF NOT EXISTS %s" % (database))
        print("create database query:", sql)
        self.cur.execute(sql)
        logs = self.cur.fetch_logs()
        print("logs: ", logs)

    def check_if_table_matches_partitioning(self, table, partition_level):
        """Check if table partitioning and given partition_level match.

        The goal is to prevent human error when passing existing
        table name and partition level that does not match 
        the partition information found in this table schema.
        """
        sql = ("DESCRIBE %s" % (table))
        print("Describe table query:", sql)
        self.cur.execute(sql)
        table_description = self.cur.fetchall()
        print(table_description)

        partitioning_header_line_index = [
            idx for idx, item in enumerate(table_description)
            if item == ('# Partition Information', None, None)][0]

        parts = [item[0]
                 for item in table_description[partitioning_header_line_index:]
                 if item[0].startswith('part') and item[1] == 'string']

        check_partitions_matching(parts=parts, partition_level=partition_level)
        # find second empty tuple
        # take items following it
        # check they are part_x and in order

    def use_database(self, database):
        """Use database.

        This function is basically changing state of the running session.
        After calling this function, all table-related functions are
        assuming that table exists in the database that "we are using now".
        """
        sql = ("USE %s" % (database))
        print("use database query:", sql)
        self.cur.execute(sql)
        logs = self.cur.fetch_logs()
        print("logs: ", logs)

    def show_tables(self):
        """Show tables in the database that "we are using now"."""
        sql = ("SHOW TABLES")
        print("Show tables query:", sql)
        self.cur.execute(sql)
        table_tuples = self.cur.fetchall()

        # the result is given as single value tuples
        # and needs to be unpacked
        if table_tuples:
            tables = [item[0] for item in table_tuples]
        else:
            tables = []
        print('tables:', tables)

        return tables

    def describe_formatted(self, table):
        """Provide formatted desctiption of hive table."""
        sql = ("DESCRIBE FORMATTED %s" % (table))
        print("Describe table query:", sql)
        self.cur.execute(sql)
        table_description = self.cur.fetchall()
        print('table_description:', table_description)

        return table_description

    def get_table_location(self, table):
        """Get table location based on formatted description of hive table."""
        description = self.describe_formatted(table)
        # Find a tuple where location is mentioned
        table_path = [item[1]
                      for item in description if 'Location' in item[0]][0]
        # weirdly hive represents prefix with single slash
        # which needs to be corrected to 3 slashes to match hadoop output
        if 'maprfs:/' in table_path and 'maprfs:///' not in table_path:
            table_path = table_path.replace('maprfs:/', 'maprfs:///')
        elif 'hdfs:/' in table_path and 'hdfs:///' not in table_path:
            table_path = table_path.replace('hdfs:/', 'hdfs:///')
        else:
            pass

        return table_path

    def create_table_if_not_exists(self, table, schema, location, partition_level):
        """Create table if not exists.

        Note that if table already exists the schema changes 
        between existing table and new query are ignored.

        Args:
            table (str): table name
            schema (str): path to the schema file, used for type information
            location (str): path to hdfs directory to refer to external data
            partition_level (str): '' upto 'ymwdh', used for adding 
                partitioning columns to the table
        """
        sql = self._build_create_table_query(table, schema, location,
                                             partition_level)
        print("create table query:", sql)
        self.cur.execute(sql)
        logs = self.cur.fetch_logs()
        print("logs: ", logs)

    def refresh_table(self, table):
        """Update hive metastore for the table to include newly added data."""
        sql = ('MSCK REPAIR TABLE %s' % (table))
        print("refresh table query:", sql)
        self.cur.execute(sql)
        logs = self.cur.fetch_logs()
        print("logs: ", logs)

    def _build_create_table_query(self, table, schema, location, partition_level):
        """Build query string for table creation."""
        types = {
            "string": "string",
            "short": "smallint",
            "integer": "int",
            "long": "bigint",
            "float": "float",
            "double": "double",
            "decimal": "decimal",
            "timestamp": "timestamp",
            "boolean": "boolean"
        }

        #name = "evc_deststn"
        #schema = "deststn_2013_schema.json"
        #location = "/expressdelivery/master/OPMS/evc_deststn"
        #parts = []
        if partition_level == "":
            parts = []
        elif partition_level == "y":
            parts = ["part_year"]
        elif (partition_level == "ym"):
            parts = ["part_year", "part_month"]
        elif partition_level == "ymw":
            parts = ["part_year", "part_month", "part_week"]
        elif partition_level == "ymwd":
            parts = ["part_year", "part_month", "part_week", "part_day"]
        elif partition_level == "ymwdh":
            parts = ["part_year", "part_month",
                     "part_week", "part_day", "part_hour"]
        else:
            raise Exception("Unknown partition %s" % partition_level)

        f = open(schema, "r")
        lines = f.readlines()
        f.close()

        js_string = "".join(lines)
        schema = json.loads(js_string)
        items = schema["schema"]
        definitions = ["%s %s%s" % (child["name"].lower(),
                                    types[child["type"]],
                                    pattern_by_type(child)) for child in items]

    # TIMESTAMP
        parts_sql = ",".join(["%s STRING" % part for part in parts])

        if parts:
            sql = ("CREATE EXTERNAL TABLE IF NOT EXISTS %s (%s) "
                   "PARTITIONED BY (%s) "
                   "STORED AS PARQUET "
                   "LOCATION '%s'" % (table, ",".join(definitions), parts_sql,
                                      location))
        else:
            sql = ("CREATE EXTERNAL TABLE IF NOT EXISTS %s (%s) "
                   "STORED AS PARQUET "
                   "LOCATION '%s'" % (table, ",".join(definitions),
                                      location))

        return sql


def pattern_by_type(item):
    """Return pattern enclosed in parenthesis if type is 'decimal'.

    This function is basically made for correcting the mismatch in
    Decimal precision and scale definition in schema vs. in HiveQL.
    """
    if item["type"] == "decimal":
        return "(%s)" % item["pattern"]
    else:
        return ""


def check_partitions_matching(parts, partition_level):
    """Check if the table partition definition matches partitioning level."""
    if (('y' in partition_level and 'part_year' not in parts)
        or ('m' in partition_level and 'part_month' not in parts)
        or ('w' in partition_level and 'part_week' not in parts)
        or ('d' in partition_level and 'part_day' not in parts)
        or ('h' in partition_level and 'part_hour' not in parts)
        or ('y' not in partition_level and 'part_year' in parts)
        or ('m' not in partition_level and 'part_month' in parts)
        or ('w' not in partition_level and 'part_week' in parts)
        or ('d' not in partition_level and 'part_day' in parts)
            or ('h' not in partition_level and 'part_hour' in parts)):
        raise Exception(
            'partitions {} not matching "{}" partition_level'.format(
                parts, partition_level))
