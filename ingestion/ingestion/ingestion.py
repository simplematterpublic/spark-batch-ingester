import os
import glob
import uuid
import datetime
import subprocess

import ingestion.util as util
import ingestion.hive_manager as hv


def create_dirs(path):
    """Create the directories along the path."""
    if not os.path.exists(path):
        os.makedirs(path)


def execute_hadoop_ls(source_dir):
    """Execute hadoop ls as a subprocess.

    Note that if input has prefix hdfs:/// or maprfs:///
    it is also reflected into the output paths.

    Args:
        source_dir (str): path to source directory to be listed
    Returns:
        files (list(str)): list of file paths
    """
    print('executing hadoop ls')
    # probably -R not needed any more
    command = ['hadoop', 'fs', '-ls', source_dir]
    hadoop_ls_proc = subprocess.Popen(command, stdout=subprocess.PIPE)

    output = hadoop_ls_proc.stdout

    files = get_file_list_from_hadoop_output(output)

    return files


def execute_posix_ls(source_dir):
    """Execute ls on locally available file system.

    This function is useful as a substitute of 'execute_hadoop_ls'
    function when working in local mode.

    Args:
        source_dir (str): path to source directory to be listed
    Returns:
        files (list(str)): list of file paths
    """
    print('executing posix ls')
    print(source_dir.rstrip('/') + '/*')
    files = [path.replace('\\', '/')
             for path in sorted(glob.glob(source_dir.rstrip('/') + '/*'))]

    return files


def get_file_list_from_hadoop_output(hadoop_output):
    """Get file names from full paths listed by hadoop ls.

    Unlike the GNU ls which outputs just names of contained items,
    hadoop ls outputs full paths of those.

    Args:
        hadoop_output (list(str)): list of hadoop output lines
    Returns:
        file_list (list(str)): list of the file names
    """
    output_lines = [line.strip().decode() for line in hadoop_output
                    if len(line.strip())]
    output_matrix = [line.split(" ") for line in output_lines]
    file_list = [row[-1].strip() for row in output_matrix if len(row) > 6]

    return file_list


def get_unix_timestamp_now():
    """Return current datetime as unix timestamp in milliseconds."""
    ts = get_unix_timestamp(None)

    return ts


def get_unix_timestamp(dt):
    """Return given datetime as unix timestamp in milliseconds."""
    dt = datetime.datetime.utcnow() if dt is None else dt
    # failed on python 2.7
    # ts = int(dt.timestamp() * 1000)
    delta = dt - datetime.datetime(1970, 1, 1)
    ts = int(delta.total_seconds() * 1000)

    return ts


def generate_uuid_string(with_dashes=True):
    """Generate random uuid with or without dashes.

    E.g. 4a46ed0d-0f55-4035-a03c-0799636e09d3 or 
    4a46ed0d0f554035a03c0799636e09d3, respectively.
    """
    id = uuid.uuid4()
    id_string = str(id) if with_dashes else id.hex

    return id_string


def generate_unique_name(dt=None):
    """Generate unique name consisting of [UUID]_[TIMESTAMP].

    UUID is formatted as 4a46ed0d-0f55-4035-a03c-0799636e09d3,
    TIMESTAMP is a unix timestamp in milliseconds.
    """
    uuid_part = generate_uuid_string()
    timestamp_part = str(get_unix_timestamp(dt))

    return '_'.join([uuid_part, timestamp_part])


def execute_job(name,
                source_file,
                target_path,
                file_type,
                hdfs_mode,
                master,
                spark_job_args,
                schema,
                partition_args,
                overwrite='false'):
    """Execute a single spark job.

    Here the bash command is constructed based on the given args,
    executed and return code of the subprocess running spark-job
    is returned.
    """
    print('executing job with arguments')
    for k, v in locals().items():
        print(k, v)

    target_unique_dir = ''

    # TODO: generate unique name is specific to json as
    # it is going to be merged afterwards. Move to merge.
    # if file_type == 'json':
    #     filename = os.path.basename(source_file)
    #     year, month = filename[10:14], filename[14:16]
    #     dt = datetime.datetime(year=int(year), month=int(month), day=1)
    #     target_unique_dir = (target_path.rstrip('/') + '/'
    #                          + generate_unique_name(dt))

    if overwrite == 'false':
        overwrite_args = []
    elif overwrite == 'true':
        overwrite_args = ['-overwrite', 'true']
    else:
        raise Exception('overwrite mode should be either false or true')

    if hdfs_mode:
        full_command = [
            '/opt/mapr/spark/spark-2.2.1/bin/spark-submit',
            '--name', 'data-ingestion' + '-' + name,
            '--master', master,
            # 'yarn' if YARN else 'local',
            '--deploy-mode', 'cluster' if master == 'yarn' else 'client',
            '--class', 'datalake.Main',
            '--files' if master == 'yarn' else '',
            (schema + '#schema.json') if master == 'yarn' else '',
            # '--driver-java-options', '-Dconfig=sparkjob.conf',
            '/app/spark-job/spark-batch-ingester-1.0-SNAPSHOT.jar'
        ] + [
            '-master', 'cluster',
            '-source', source_file,
            '-target', target_unique_dir if target_unique_dir else target_path,
            '-type', file_type
        ] + ([
            # the name is always 'schema.json' when running on yarn
            # because it is already renamed when passed as a file
            '-schema', 'schema.json'
        ] if file_type == 'csv' and schema else []
        ) + spark_job_args + partition_args + overwrite_args
    else:
        full_command = [
            'java', '-jar',
            # '-Dconfig=' + spark_job_conf,
            'spark-job/spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar'
        ] + [
            '-master', 'local',
            '-source', source_file,
            '-target', target_path,
            '-type', file_type,
            '-schema', schema
        ] + ([
            # in local mode we use original schema file
            '-schema', schema
        ] if file_type == 'csv' and schema else []
        ) + spark_job_args + partition_args + overwrite_args

    print('Executing command:', ' '.join(full_command), flush=True)

    proc = subprocess.Popen(full_command,
                            stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE,
                            universal_newlines=True)
    stdout, stderr = proc.communicate()
    print(stdout, flush=True)
    # create_dirs('stderr')
    # with open('stderr/' + os.path.basename(source_file), 'w') as proc_output:
    #     proc_output.write(output[1])

    return proc.returncode


def checkpoint_diff(checkpoint_path, file_list):
    """Perform diff between the staging files and checkpoint.

    Args:
        checkpoint_path (str): path to checkpoint file
        file_list (list(str)): list of files available under staging directory
    Returns:
        unprocessed_files (list(str)): list of files that are not yet processed
    """
    if os.path.exists(checkpoint_path):
        # read checkpoint file and diff names
        with open(checkpoint_path, 'r') as fh:
            checkpointed_files = [line.strip() for line in fh]
            print('Checkpointed files:', len(checkpointed_files))
            print('----', checkpointed_files[0:4])
    else:
        checkpointed_files = []

    unprocessed_files = [file for file in file_list
                         if file not in checkpointed_files]
    print('Unprocessed files (showing first 4 items):', len(unprocessed_files))
    print('----', unprocessed_files[0:4])

    return unprocessed_files


def produce_target_path(source_file, source_dir, target_dir):
    """Replicate relative subdirectories of source under target directory.

    Take source directory and source file path, identify the subdirectories
    of the source and create the same subdirectories under target directory.

    Args:
        source_file (str): Full path of source file
        source_dir (str): "Umbrella" source directory (provided through args)
        target_dir (str): "Umbrella" target directory (provided through args)
    Returns:
        target_dir_path: full path of target directory structure
    """
    source_dir_path = os.path.dirname(source_file)
    subdir_name = source_dir_path.replace(source_dir, '').strip('/')
    target_dir_path = target_dir.rstrip('/') + '/' + subdir_name

    return target_dir_path


def check_if_target_matches_table(args):
    """Check if table location and ingestion target match.

    The goal of this check is to prevent two hive tables referencing
    the same location and potentially introducing the partitioning
    structure mismatch. This check is done before actual ingestion starts.

    Args:
        args (Argparse): Object containing all command line arguments passed.
            For details about arguments please refer to the usage section
            in README.
    """
    hive = hv.HiveConnector(host=args.host,
                            port=args.port,
                            username=args.username,
                            password=args.password,
                            auth='CUSTOM')
    hive.create_database_if_not_exists(args.database)
    hive.use_database(args.database)

    # show tables to check if table already exists
    table_list = hive.show_tables()
    file_list = execute_hadoop_ls(args.target)
    print('file_list: ', file_list)
    # if table does not exist and the target folder is not empty
    # then raise exception
    if args.table not in table_list and len(file_list) > 0:
        raise Exception(
            "creation of new table on existing data is not allowed")

    # if table exists, then extract its location and check
    # whether it matches the target path
    if args.table in table_list:
        table_path = hive.get_table_location(args.table)

        print('table_path:', table_path)
        print('target:', args.target)
        if table_path != args.target:
            raise Exception("target ({}) is not matching the table location ({})".format(
                args.target, table_path))


def run_ingestion(args):
    """Execute specific ingestion based on file type given in args.

    This function will loop over all unprocessed files and process them
    keeping track of the return value, checkpointing each processed
    file and failing in case the return value is not 0.

    Args:
        args (Argparse): Object containing all command line arguments passed.
            For details about arguments please refer to the usage section
            in README.
    """
    hdfs_mode = (args.source.startswith('maprfs://')
                 or args.source.startswith('hdfs://'))
    file_list = (execute_hadoop_ls(args.source)
                 if hdfs_mode
                 else execute_posix_ls(args.source))

    check_if_target_matches_table(args)

    unprocessed_files = checkpoint_diff(args.checkpoint, file_list)
    target_path_list = [produce_target_path(source_file, args.source, args.target)
                        for source_file in unprocessed_files]
    schema = None
    if args.file_type == 'csv':
        spark_job_args = [
            '-sep', '"' + args.sep + '"',
            '-header', str(args.header).lower(),
            '-ignoreLeadingWhiteSpace', str(args.ltrim).lower(),
            '-ignoreTrailingWhiteSpace', str(args.rtrim).lower()
        ]
        schema = args.schema
    elif args.file_type == 'json':
        spark_job_args = [
            '-primitivesAsString', str(args.primitives_as_string).lower(),
            '-multiline', str(args.multiline).lower()
        ]
    elif args.file_type == 'xml':
        spark_job_args = []

    # importing partitioning logic dynamically
    partitioning_func = util.get_partitioner_function(
        custom_partitioner=args.custom_partitioner,
        partition_level=args.partition_level)

    # print(partitioning_func(partition_level=args.partition_level,
    #                         filename='hscode_shipment_20180205.csv'))

    # traversing over each unprocessed file
    # providing its respective target path
    for source_path, target_path in zip(unprocessed_files, target_path_list):
        filename = os.path.basename(source_path)
        partition_args = partitioning_func(partition_level=args.partition_level,
                                           filename=filename)
        # if produced partitions does not match level, exception will be raised
        util.check_partition_args(partition_args, args.partition_level)

        return_code = execute_job(name=args.name,
                                  source_file=source_path,
                                  target_path=target_path,
                                  file_type=args.file_type,
                                  hdfs_mode=hdfs_mode,
                                  master=args.master,
                                  spark_job_args=spark_job_args,
                                  schema=schema,
                                  partition_args=partition_args,
                                  overwrite="false")
        # if successful, add name to the checkpoint
        if return_code == 0:
            # if checkpoint path does not exist, create one
            create_dirs(os.path.dirname(args.checkpoint))
            with open(args.checkpoint, 'a') as updated_checkpoint_file:
                updated_checkpoint_file.readline
                updated_checkpoint_file.write(source_path)
                updated_checkpoint_file.write('\n')
            print('Processed', source_path)

            # if ingestion was done in hdfs mode, then we need to
            # create and/or refresh respective hive table
            if hdfs_mode:
                if args.file_type == 'csv':
                    hive = hv.HiveConnector(host=args.host,
                                            port=args.port,
                                            username=args.username,
                                            password=args.password,
                                            auth='CUSTOM')
                    hive.create_database_if_not_exists(args.database)
                    hive.use_database(args.database)
                    hive.create_table_if_not_exists(args.table,
                                                    args.schema,
                                                    args.target,
                                                    args.partition_level)
                    hive.refresh_table(args.table)
        else:
            print('Failed', source_path)
        print()


def init_hive(args):
    """Initialize hive database and table based on args.

    This function is used from an alternative entry point that is
    more convenient to prepare hive table without performing ingestion.

    Args:
        args (Argparse): Object containing all command line arguments passed.
            For details about arguments please refer to the usage section
            in README.
    """
    is_target_on_hdfs = (args.target.startswith('maprfs://')
                         or args.target.startswith('hdfs://'))

    if not is_target_on_hdfs:
        raise Exception("hive table can only be initialized on hdfs path")

    hive = hv.HiveConnector(host=args.host,
                            port=args.port,
                            username=args.username,
                            password=args.password,
                            auth='CUSTOM')
    hive.create_database_if_not_exists(args.database)
    hive.use_database(args.database)
    hive.create_table_if_not_exists(args.table,
                                    args.schema,
                                    args.target,
                                    args.partition_level)


def run_metadata_ingestion(args):
    """Execute metadata ingestion based on args.

    This function is used from an alternative entry point that is
    running the ingestion in overwrite mode, without checkpointing 
    and without partitioning. Convenient for application metadata,
    e.g. static data as lookup tables, enums and descriptions.
    Such tables rarely need the update and they usually don't have
    any timestamps.
    Args:
        args (Argparse): Object containing all command line arguments passed.
            For details about arguments please refer to the usage section
            in README.
        """
    hdfs_mode = (args.source.startswith('maprfs://')
                 or args.source.startswith('hdfs://'))
    file_list = (execute_hadoop_ls(args.source)
                 if hdfs_mode
                 else execute_posix_ls(args.source))
    # in metadata case we take all files without any checkpointing
    # as they are ingested in overwrite mode anyway
    target_path_list = [produce_target_path(source_file, args.source, args.target)
                        for source_file in file_list]

    schema = None
    if args.file_type == 'csv':
        spark_job_args = [
            '-sep', '"' + args.sep + '"',
            '-header', str(args.header).lower(),
            '-ignoreLeadingWhiteSpace', str(args.ltrim).lower(),
            '-ignoreTrailingWhiteSpace', str(args.rtrim).lower()
        ]
        schema = args.schema

    # traversing over each file providing its respective target path
    for source_path, target_path in zip(file_list, target_path_list):
        filename = os.path.basename(source_path)

        return_code = execute_job(name=args.name,
                                  source_file=source_path,
                                  target_path=target_path,
                                  file_type=args.file_type,
                                  hdfs_mode=hdfs_mode,
                                  master=args.master,
                                  spark_job_args=spark_job_args,
                                  schema=schema,
                                  partition_args=[],
                                  overwrite="true")

        if return_code == 0:
            print('Processed', source_path)
            # if ingestion was done in hdfs mode, then we need to
            # create and/or refresh respective hive table
            if hdfs_mode:
                if args.file_type == 'csv':
                    hive = hv.HiveConnector(host=args.host,
                                            port=args.port,
                                            username=args.username,
                                            password=args.password,
                                            auth='CUSTOM')
                    hive.create_database_if_not_exists(args.database)
                    hive.use_database(args.database)
                    hive.create_table_if_not_exists(args.table,
                                                    args.schema,
                                                    args.target,
                                                    partition_level='')
                    hive.refresh_table(args.table)
        else:
            print('Failed', source_path)
        print()
