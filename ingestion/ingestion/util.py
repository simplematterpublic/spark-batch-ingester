import importlib
import pandas as pd


def compute_week_of_month(dt):
    """Compute week-of-month.

    Computation of week-of-month depends on calendar defaults.
    This implementation assumes the following defaults:
    * start of the week is Monday
    * if the week transitions between two months, then the last few days
        are considered as 5th or 6th week, then the remaining days are
        considered as 1st week because they are in the beginning of the month.
    """
    ts = pd.Timestamp(dt)
    ts_start = pd.Timestamp(year=ts.year, month=ts.month, day=1)

    first_week_of_month = ts_start.week

    # When first week of month is still stuck in last years count
    # and the current week count is reset, then we need to add last year's
    # amount of weeks (first_week_of_month, which is 52 or more) to
    # compute the difference correctly
    if first_week_of_month >= 47 and ts.week <= 6:
        current_week = ts.week + (first_week_of_month
                                  if first_week_of_month > 52 else 52)
    else:
        current_week = ts.week

    # Here we compute the difference between current week and
    # the first week of the month.
    week_of_month = current_week - first_week_of_month + 1

    # for debugging
    # print(ts.date(), first_week_of_month, ts.week, current_week,
    #       week_of_month)

    return week_of_month


def compute_partition(partition_level, filename=''):
    """Compute partition based on current time.

    Args:
        filename (str): required in case of custom logic of partition
            computation, not used here (useful for historical import)
        partition (str): a sequence of characters in the following
            order: 'ymwdh'. Actual partitioning level is controlled
            by the starting subsequence of the given string, e.g.
            for monthly partitioning the string would be 'ym',
            for daily partitioning - 'ymwd', for no partitioning
            it would be empty string.
    Returns:
        partition_args (list(str)): list of strings directly usable as
            partitioning arguments for spark job."""
    print("executing default compute_partition")
    now = pd.Timestamp.now()
    if partition_level == "":
        parts = []
    if partition_level == "y":
        parts = ["-year", str(now.year)]
    elif (partition_level == "ym"):
        parts = ["-year", str(now.year),
                 "-month", '{:02d}'.format(now.month)]
    elif partition_level == "ymw":
        parts = ["-year", str(now.year),
                 "-month", '{:02d}'.format(now.month),
                 "-week", str(compute_week_of_month(now))]
    elif partition_level == "ymwd":
        parts = ["-year", str(now.year),
                 "-month", '{:02d}'.format(now.month),
                 "-week", str(compute_week_of_month(now)),
                 "-day", '{:02d}'.format(now.day)]
    elif partition_level == "ymwdh":
        parts = ["-year", str(now.year),
                 "-month", '{:02d}'.format(now.month),
                 "-week", str(compute_week_of_month(now)),
                 "-day", '{:02d}'.format(now.day),
                 "-hour", '{:02d}'.format(now.hour)]
    else:
        raise Exception("Unknown partition_level %s" % partition_level)

    return parts


def get_partitioner_function(custom_partitioner, partition_level):
    """Import partitioning logic dynamically."""
    if (custom_partitioner):
        partitioning_module = importlib.import_module(custom_partitioner)
        partitioning_func = partitioning_module.compute_partition
    else:
        partitioning_func = compute_partition

    return partitioning_func


def check_partition_args(partition_args, partition_level):
    """Check if generated partition_args matches partition_level.

    This function is covering for possible mismatch introduced by
    the custom partitioner imported dynamically that may return
    partitioning args not matching partition level.

    Args:
        partition_args: the list of arguments produced by 
            default or custom partitioner
        partition_level: the sequence of chars from 'y' upto 'ymwdh'
            indicating the level of partitioning

    Raises:
        Exception: if the argument is missing based on level or 
            number of arguments and their respective values are not
            matching the level.
    """
    if (('y' in partition_level and '-year' not in partition_args)
        or ('m' in partition_level and '-month' not in partition_args)
        or ('w' in partition_level and '-week' not in partition_args)
        or ('d' in partition_level and '-day' not in partition_args)
        or ('h' in partition_level and '-hour' not in partition_args)
        or ('y' not in partition_level and '-year' in partition_args)
        or ('m' not in partition_level and '-month' in partition_args)
        or ('w' not in partition_level and '-week' in partition_args)
        or ('d' not in partition_level and '-day' in partition_args)
            or ('h' not in partition_level and '-hour' in partition_args)):
        raise Exception(
            'partition_args {} not matching "{}" partition_level'.format(
                partition_args, partition_level))

    if len(partition_args) != 2 * len(partition_level):
        raise Exception('The number of optional arguments and their values '
                        'do not match expected level of partitioning')

    return True
