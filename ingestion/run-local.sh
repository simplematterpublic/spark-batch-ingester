java -jar -Dconfig="../customconfig.properties" "../target/spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar" \
    -master local \
    -source ../data/playground/staging/OPMS/evc_deststn_2016/OPMS_BE_evc_add_2016_nov_stn_0_0.txt \
    -target ../data/playground/master/OPMS/evc_deststn/ \
    -type csv \
    -schema schema.json \
    -year 2018 \
    -month 02
