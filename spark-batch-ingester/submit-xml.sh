$SPARK_HOME/bin/spark-submit \
  --name "spark-data-ingestion" \
  --master local \
  --deploy-mode client \
  --driver-java-options -Dconfig=xml.conf \
  target/spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar \
    -master local \
    -source source/sample.xml \
    -target source \
    -type xml
