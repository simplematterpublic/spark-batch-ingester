package datalake;

import datalake.spark.processors.Processor;
import datalake.spark.processors.csv.CSVProcessor;
import datalake.spark.processors.json.JSONProcessor;
import datalake.spark.processors.xml.XMLProcessor;
import datalake.spark.schema.Schema;
import org.apache.commons.cli.*;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.SparkSession;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {

        final Options options = new Options();

        final Option modeOption = new Option("master", "master", true, "Master (cluster or local)");
        modeOption.setRequired(true);
        options.addOption(modeOption);

        final Option sourceOption = new Option("source", "source", true, "Source path");
        sourceOption.setRequired(true);
        options.addOption(sourceOption);

        final Option targetOption = new Option("target", "target", true, "Target path");
        targetOption.setRequired(true);
        options.addOption(targetOption);

        final Option schemaOption = new Option("schema", "schema", true, "Schema path");
        schemaOption.setRequired(false);
        options.addOption(schemaOption);

        final Option typeOption = new Option("type", "type", true, "Type (csv, json or xml)");
        typeOption.setRequired(true);
        options.addOption(typeOption);

        final Option partYearOption = new Option("year", "year", true, "Part Year");
        partYearOption.setRequired(false);
        options.addOption(partYearOption);

        final Option partMonthOption = new Option("month", "month", true, "Part Month");
        partMonthOption.setRequired(false);
        options.addOption(partMonthOption);

        final Option partWeekOption = new Option("week", "week", true, "Part Week");
        partWeekOption.setRequired(false);
        options.addOption(partWeekOption);

        final Option partDayOption = new Option("day", "day", true, "Part Day");
        partDayOption.setRequired(false);
        options.addOption(partDayOption);

        final Option partHourOption = new Option("hour", "hour", true, "Part Hour");
        partHourOption.setRequired(false);
        options.addOption(partHourOption);

        final Option overwriteOption = new Option("overwrite", "overwrite", true, "Save mode overwrite (true of false)");
        overwriteOption.setRequired(false);
        options.addOption(overwriteOption);

        // csv options
        final Option sepOption = new Option("sep", "sep", true, "csv separator");
        sepOption.setRequired(false);
        options.addOption(sepOption);

        final Option headerOption = new Option("header", "header", true, "csv header");
        headerOption.setRequired(false);
        options.addOption(headerOption);

        final Option ignoreLeadingWhiteSpaceOption = new Option("ignoreLeadingWhiteSpace", "ignoreLeadingWhiteSpace", true, "ignore leading whitespace in csv cells");
        ignoreLeadingWhiteSpaceOption.setRequired(false);
        options.addOption(ignoreLeadingWhiteSpaceOption);

        final Option ignoreTrailingWhiteSpace = new Option("ignoreTrailingWhiteSpace", "ignoreTrailingWhiteSpace", true, "ignore trailing whitespace in csv cells");
        ignoreTrailingWhiteSpace.setRequired(false);
        options.addOption(ignoreTrailingWhiteSpace);


//        final Option sepOption = new Option("sep", "sep", true, "csv separator");
//        sepOption.setRequired(false);
//        options.addOption(sepOption);
//
//        final Option sepOption = new Option("sep", "sep", true, "csv separator");
//        sepOption.setRequired(false);
//        options.addOption(sepOption);
//
//        final Option sepOption = new Option("sep", "sep", true, "csv separator");
//        sepOption.setRequired(false);
//        options.addOption(sepOption);


        final CommandLineParser parser = new BasicParser();
        final HelpFormatter formatter = new HelpFormatter();

        try {
            for (int i = 0; i < args.length; i++) {
                System.out.print(args[i]);
                System.out.print(" ");
                if (i % 2 == 1) {
                    System.out.println();
                }
            }
            final CommandLine commandLine = parser.parse(options, args);
            final String master = commandLine.getOptionValue("master");
            final String source = commandLine.getOptionValue("source");
            final String target = commandLine.getOptionValue("target");
            final String type = commandLine.getOptionValue("type");
            final String schema = commandLine.getOptionValue("schema");
            final String partYear = commandLine.getOptionValue("year");
            final String partMonth = commandLine.getOptionValue("month");
            final String partWeek = commandLine.getOptionValue("week");
            final String partDay = commandLine.getOptionValue("day");
            final String partHour = commandLine.getOptionValue("hour");
            final String overwrite = commandLine.getOptionValue("overwrite", "false");

            Properties properties;

            if (type.equals("csv")) {
                properties = new Properties();
                properties.setProperty("sep", commandLine.getOptionValue("sep"));
                properties.setProperty("header", commandLine.getOptionValue("header"));
                properties.setProperty("ignoreLeadingWhiteSpace", commandLine.getOptionValue("ignoreLeadingWhiteSpace"));
                properties.setProperty("ignoreTrailingWhiteSpace", commandLine.getOptionValue("ignoreTrailingWhiteSpace"));
            } else if (type.equals("json")) {
                properties = new Properties();
                properties.setProperty("primitivesAsString", commandLine.getOptionValue("primitivesAsString"));
                properties.setProperty("multiline", commandLine.getOptionValue("multiline"));
            } else if (type.equals("xml")) {
                properties = new Properties();
            } else {
                throw new RuntimeException("type of data source should be explicitly set to csv, json or xml");
            }

//            final Properties properties = loadConfig(System.getProperty("config"));

            final SparkSession sparkSession;

            if ("cluster".toLowerCase().equals(master.trim().toLowerCase())) {
                sparkSession = SparkSession.builder().getOrCreate();
            } else {
                final SparkConf sparkConf = new SparkConf().setAppName("local").setMaster("local");
                final SparkContext sparkContext = new SparkContext(sparkConf);
                sparkSession = new SparkSession(sparkContext).newSession();
            }

            final Processor processor;
            if ("csv".toLowerCase().equals(type.toLowerCase())) {

                if (!(overwrite.toLowerCase().equals("true") ||
                        overwrite.toLowerCase().equals("false"))) {
                    throw new RuntimeException("overwrite value should be either false or true");
                }

                final boolean isSaveModeOverwrite = "true".equals(overwrite.toLowerCase());

                processor = new CSVProcessor(properties, source.trim(), target.trim(), isSaveModeOverwrite);
                if (schema != null) {
                    processor.setSchema(Schema.get(schema.trim()));
                }
            } else if ("json".toLowerCase().equals(type.toLowerCase())) {

                processor = new JSONProcessor(properties, source.trim(), target.trim());
            } else if ("xml".toLowerCase().equals(type.toLowerCase())) {

                processor = new XMLProcessor(properties, source.trim(), target.trim());
            } else {
                throw new RuntimeException("type of data source should be explicitly set to csv, json or xml");
            }

            processor.process(sparkSession, partYear == null ? null : partYear.trim(),
                    partMonth == null ? null : partMonth.trim(), partWeek == null ? null : partWeek.trim(),
                    partDay == null  ? null : partDay.trim(), partHour == null ? null : partHour.trim());

            sparkSession.close();

        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("spark-batch-ingester", options);
            System.exit(1);
        }
    }

    private static Properties loadConfig(final String config) {

        final Properties props = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(config);
            props.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return props;
    }

}
