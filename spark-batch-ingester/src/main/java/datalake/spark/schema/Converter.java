package datalake.spark.schema;

import datalake.spark.schema.Schema;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class Converter {

    private static String toString(final String value, final String defaultValue) {

        return value == null ? defaultValue : value;
    }

    private static Short toShort(final String value, final String defaultValue) {

        return value == null ? defaultValue == null ? 0 : Short.parseShort(defaultValue) : Short.parseShort(value);
    }

    private static Integer toInteger(final String value, final String defaultValue) {

        return value == null ? defaultValue == null ? 0 : Integer.parseInt(defaultValue) : Integer.parseInt(value);
    }

    private static Long toLong(final String value, final String defaultValue) {

        return value == null ? defaultValue == null ? 0L : Long.parseLong(defaultValue) : Long.parseLong(value);
    }

    private static Float toFloat(final String value, final String defaultValue) {

        return value == null ? defaultValue == null ? 0F : Float.parseFloat(defaultValue) : Float.parseFloat(value);
    }

    private static Double toDouble(final String value, final String defaultValue) {

        return value == null ? defaultValue == null ? 0D : Double.parseDouble(defaultValue) : Double.parseDouble(value);
    }

    private static BigInteger toBigInteger(final String value, final String defaultValue) {

        return value == null ? defaultValue == null ? null : new BigInteger(defaultValue) : new BigInteger(value);
    }

    private static BigDecimal toBigDecimal(final String value, final String defaultValue) {

        return value == null ? defaultValue == null ? null : new BigDecimal(defaultValue) : new BigDecimal(value);
    }

    private static Boolean toBoolean(final String value, final String defaultValue) {

        if (value == null) {
            if (defaultValue == null) {
                return false;
            } else if ("1".equals(defaultValue) || "true".toLowerCase().equals(defaultValue.toLowerCase())) {
                return true;
            } else {
                return false;
            }
        } else if ("1".equals(value) || "true".toLowerCase().equals(value.toLowerCase())) {
            return true;
        } else {
            return false;
        }
    }

    private static Timestamp toTimestamp(final String value, final String defaultValue, final String pattern) throws Exception {

        if (value == null) {
            if (defaultValue == null) {
                return null;
            } else {
                final SimpleDateFormat format = new SimpleDateFormat(pattern);
                return new Timestamp(format.parse(defaultValue).getTime());
            }
        } else {
            final SimpleDateFormat format = new SimpleDateFormat(pattern);
            return new Timestamp(format.parse(value).getTime());
        }
    }

    private static Object convert(final String type, final String value, final String defaultValue, final String pattern)
            throws Exception {

        if ("boolean".toLowerCase().equals(type.toLowerCase())) {
            return toBoolean(value, defaultValue);
        } else if ("string".toLowerCase().equals(type.toLowerCase())) {
            return toString(value, defaultValue);
        } else if ("short".toLowerCase().equals(type.toLowerCase())) {
            return toShort(value, defaultValue);
        } else if ("integer".toLowerCase().equals(type.toLowerCase())) {
            return toInteger(value, defaultValue);
        } else if ("long".toLowerCase().equals(type.toLowerCase())) {
            return toLong(value, defaultValue);
        } else if ("float".toLowerCase().equals(type.toLowerCase())) {
            return toFloat(value, defaultValue);
        } else if ("double".toLowerCase().equals(type.toLowerCase())) {
            return toDouble(value, defaultValue);
        } else if ("decimal".toLowerCase().equals(type.toLowerCase())) {
            return toBigDecimal(value, defaultValue);
        } else if ("date".toLowerCase().equals(type.toLowerCase())) {
            return toTimestamp(value, defaultValue, pattern);
        } else if ("timestamp".toLowerCase().equals(type.toLowerCase())) {
            return toTimestamp(value, defaultValue, pattern);
        } else {
            throw new RuntimeException("Type not supported: " + type);
        }
    }

    public static Row convert(final Row row, final Schema schema) throws Exception{

        final List<Object> values = new ArrayList<>();
        for (int i = 0; i < row.size(); i++) {
            final Schema.Field field = schema.getField(i);
            values.add(convert(field.getType(), row.getString(i), field.getValue(), field.getPattern()));
        }
        return RowFactory.create(values.toArray());
    }
}
