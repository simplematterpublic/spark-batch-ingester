package datalake.spark.schema;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class Schema implements Serializable {

    private final List<Field> fields;

    public Schema() {

        this.fields = new ArrayList<>();
    }

    public void addField(final String name, final String type, final String value, final String pattern) {

        addField(new Field(name, type, value, pattern));
    }

    public void addField(final Field field) {

        fields.add(field);
    }

    public Field getField(final int index) {

        return fields.get(index);
    }

    public int getSize() {

        return fields.size();
    }

    private int extractPrecision(String pattern) {
        String[] tokens = getTokensFromPattern(pattern);

        int precision = Integer.valueOf(tokens[0]);

        return precision;
    }

    private int extractScale(String pattern) {
        String[] tokens = getTokensFromPattern(pattern);

        int scale = Integer.valueOf(tokens[1]);

        return scale;
    }

    private String[] getTokensFromPattern(String pattern) {
        String[] tokens = pattern.split(",", 2);

        if (tokens.length < 2) {
            throw new RuntimeException("Pattern should contain precision and scale delimited by comma");
        }
        return tokens;
    }


    private StructField convertFieldToStructField(Field field) {
        System.out.println(field.toString());
        if ("boolean".toLowerCase().equals(field.getType().toLowerCase())) {
            return DataTypes.createStructField(field.getName(), DataTypes.BooleanType, true);
        } else if ("string".toLowerCase().equals(field.getType().toLowerCase())) {
            return DataTypes.createStructField(field.getName(), DataTypes.StringType, true);
        } else if ("short".toLowerCase().equals(field.getType().toLowerCase())) {
            return DataTypes.createStructField(field.getName(), DataTypes.ShortType, true);
        } else if ("integer".toLowerCase().equals(field.getType().toLowerCase())) {
            return DataTypes.createStructField(field.getName(), DataTypes.IntegerType, true);
        } else if ("long".toLowerCase().equals(field.getType().toLowerCase())) {
            return DataTypes.createStructField(field.getName(), DataTypes.LongType, true);
        } else if ("float".toLowerCase().equals(field.getType().toLowerCase())) {
            return DataTypes.createStructField(field.getName(), DataTypes.FloatType, true);
        } else if ("double".toLowerCase().equals(field.getType().toLowerCase())) {
            return DataTypes.createStructField(field.getName(), DataTypes.DoubleType, true);
        } else if ("decimal".toLowerCase().equals(field.getType().toLowerCase())) {
            int precision = extractPrecision(field.getPattern());
            int scale = extractScale(field.getPattern());
            return DataTypes.createStructField(field.getName(), DataTypes.createDecimalType(precision, scale), true);
        } else if ("date".toLowerCase().equals(field.getType().toLowerCase())) {
            return DataTypes.createStructField(field.getName(), DataTypes.DateType, true);
        } else if ("timestamp".toLowerCase().equals(field.getType().toLowerCase())) {
            return DataTypes.createStructField(field.getName(), DataTypes.TimestampType, true);
        } else {
            throw new RuntimeException("Field of unknown type");
        }
    }

    private void checkNamesMatch(List<String> csvNames, List<String> fieldNames) {
        List<String> inCsvOnly = csvNames.stream()
                .filter(name -> !fieldNames.contains(name))
                .collect(Collectors.toList());

        List<String> inSchemaOnly = fieldNames.stream()
                .filter(name -> !csvNames.contains(name))
                .collect(Collectors.toList());

        if (inCsvOnly.isEmpty() && inSchemaOnly.isEmpty()) {
            return;
        } else {
            String inCsvOnlyAsString = inCsvOnly.stream()
                    .reduce((String a, String b) -> a + "," + b)
                    .get();
            String inSchemaOnlyAsString = inSchemaOnly.stream()
                    .reduce((String a, String b) -> a + "," + b)
                    .get();
            throw new RuntimeException("Columns found only in csv: " + inCsvOnlyAsString + "; " +
                    "Fields found only in schema: " + inSchemaOnlyAsString);
        }
    }

    public StructType getStructTypeByName(List<String> names) {
        final List<StructField> definitions = new ArrayList<>();
        List<String> fieldNames = fields.stream().map(field -> field.getName()).collect(Collectors.toList());

        checkNamesMatch(names, fieldNames);

        for (final String name: names) {
            Optional<Field> schemaItem = fields.stream().filter(field -> field.getName().equals(name)).findFirst();
            StructField structField;
            structField = convertFieldToStructField(schemaItem.get());
            definitions.add(structField);
        }

        return DataTypes.createStructType(definitions);
    }

    public StructType getStructType() {

        final List<StructField> definitions = new ArrayList<>();
        for (final Field field: fields) {
            StructField structField = convertFieldToStructField(field);
            definitions.add(structField);
        }

        return DataTypes.createStructType(definitions);
    }

    public static class Field implements Serializable {

        private String name;
        private String type;
        private String value;
        private String pattern;

        public Field(final String name, final String type, final String value, final String pattern) {

            this.name = name;
            this.type = type;
            this.value = value;
            this.pattern = pattern;
        }

        public String getName() {
            return name;
        }

        public String getType() {
            return type;
        }

        public String getValue() {
            return value;
        }

        public String getPattern() {
            return pattern;
        }

        @Override
        public String toString() {
            return this.getName() + ", " +
                    this.getType() + ", " +
                    this.getPattern() + ", " +
                    this.getValue();
        }
    }

    public static Schema get(final String config) {

        try {
            final Schema schema = new Schema();
            final JsonParser parser = new JsonParser();
            final JsonObject jsonConfig = (JsonObject) parser.parse(new FileReader(config));
            final JsonArray jsonSchema = jsonConfig.getAsJsonArray("schema");
            final Iterator<JsonElement> fields = jsonSchema.iterator();
            while (fields.hasNext()) {
                final JsonObject element = fields.next().getAsJsonObject();
                final JsonElement nameElement = element.get("name");
                Objects.requireNonNull(nameElement, "name is required");
                final String name = nameElement.getAsString();
                final JsonElement typeElement = element.get("type");
                Objects.requireNonNull(nameElement, "type is required");
                final String type = typeElement.getAsString();
                final JsonElement valueElement = element.get("value");
                final String value = valueElement == null ? null : valueElement.getAsString();
                final JsonElement patternElement = element.get("pattern");
                final String pattern = patternElement == null ? null : patternElement.getAsString();
                schema.addField(new Schema.Field(name, type, value, pattern));
            }
            return schema;
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static void main(String args[]) throws Exception {

        final Schema schema = Schema.get("schema.json");
        System.out.println(schema);


    }
}
