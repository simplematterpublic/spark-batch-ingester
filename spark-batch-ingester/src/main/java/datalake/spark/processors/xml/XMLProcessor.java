package datalake.spark.processors.xml;

import datalake.spark.processors.Processor;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.*;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;
import org.json.XML;
import scala.Tuple2;


import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class XMLProcessor extends Processor {

    public XMLProcessor(final Properties properties, final String sourcePath, final String targetPath) {
        super(properties, sourcePath, targetPath, false);
    }

    @Override
    public void process(final SparkSession sparkSession, final String partYear, final String partMonth,
                        final String partWeek, final String partDay, final String partHour) {

        // TODO: remove hardcoding numPartitions = 1
        RDD<Tuple2<String, String>> rdd = sparkSession.sparkContext().wholeTextFiles(getSourcePath(), 1);

        JavaRDD<String> xRdd = rdd.toJavaRDD()
                .map(tuple2 -> tuple2._2())
                .map(content -> XML.toJSONObject(content))
                .map(jsonObject -> jsonObject.toString(4));

        StructField stringField = DataTypes.createStructField("json_blob_data", DataTypes.StringType, true);
        List<StructField> fields = new ArrayList<>();
        fields.add(stringField);
        StructType struct = DataTypes.createStructType(fields);

        JavaRDD<Row> rowRdd = xRdd.map(stringEntry -> RowFactory.create(stringEntry));

        Dataset<Row> jsonAsRowDF = sparkSession.createDataFrame(rowRdd, struct);

        final String finalPath;

        if (partYear != null) {
            if (partMonth != null) {
                if (partWeek != null) {
                    if (partDay != null) {
                        if (partHour != null) {
                            finalPath = getTargetPath() +
                                    "/" + "part_year=" + partYear +
                                    "/" + "part_month=" + partMonth +
                                    "/" + "part_week=" + partWeek +
                                    "/" + "part_day=" + partDay +
                                    "/" + "part_hour=" + partHour;
                        } else {
                            finalPath = getTargetPath() +
                                    "/" + "part_year=" + partYear +
                                    "/" + "part_month=" + partMonth +
                                    "/" + "part_week=" + partWeek +
                                    "/" + "part_day=" + partDay;
                        }
                    } else {
                        finalPath = getTargetPath() +
                                "/" + "part_year=" + partYear +
                                "/" + "part_month=" + partMonth +
                                "/" + "part_week=" + partWeek;
                    }
                } else {
                    finalPath = getTargetPath() +
                            "/" + "part_year=" + partYear +
                            "/" + "part_month=" + partMonth;
                }
            } else {
                finalPath = getTargetPath() + "/" + "part_year=" + partYear;
            }
        } else {
            finalPath = getTargetPath();
        }

        jsonAsRowDF.write()
                .mode(SaveMode.Append)
                .text(finalPath);

    }
}
