package datalake.spark.processors.xml;

import org.json.XML;
import org.json.JSONObject;

import java.io.Serializable;

public final class XMLToJSONConverter implements Serializable {


    public static String convert(String sourceXML) {
        final JSONObject jsonObject = XML.toJSONObject(sourceXML);
        String jsonString = jsonObject.toString(4);
        return jsonString;
    }

//    public static void convertFile(String sourceXMLPath, String targetJSONPath) {
//        String sourceXML = loadXmlAsString(sourceXMLPath);
//        String resultJSON = convert(sourceXML);
//        saveJsonString(resultJSON, targetJSONPath);
//    }
//
//    public static void saveJsonString(String jsonString, String targetFilePath) {
//        try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(targetFilePath))) {
//            writer.write(jsonString);
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static String loadXmlAsString(String sourceFilePath) {
//        byte[] bytes = new byte[0];
//        try {
//            bytes = Files.readAllBytes(Paths.get(sourceFilePath));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        return new String(bytes);
//    }
}
