package datalake.spark.processors.csv;

import datalake.spark.processors.Processor;
import datalake.spark.schema.Converter;
import datalake.spark.schema.Schema;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructField;
import org.apache.spark.sql.types.StructType;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class CSVProcessor extends Processor {

    //private final Logger logger = LogManager.getRootLogger();

    public CSVProcessor(final Properties properties, final String sourcePath, final String targetPath) {

        super(properties, sourcePath, targetPath, false);
    }

    public CSVProcessor(final Properties properties, final String sourcePath, final String targetPath, final boolean overwrite) {

        super(properties, sourcePath, targetPath, overwrite);
    }

    @Override
    public void process(final SparkSession sparkSession, final String partYear, final String partMonth,
                        final String partWeek, final String partDay, final String partHour) {

        final Properties properties = getProperties();
        //logger.info("CSV properties" + properties);
        //logger.info("CSV source path: " + getSourcePath());
        final Schema schema = getSchema();
        Dataset<Row> typedDF;

        DataFrameReader csvReader = sparkSession
                .read()
                .option("sep", properties.getProperty("sep"))
                .option("header", Boolean.parseBoolean(properties.getProperty("header")))
                .option("ignoreLeadingWhiteSpace", Boolean.parseBoolean(properties.getProperty("ignoreLeadingWhiteSpace")))
                .option("ignoreTrailingWhiteSpace", Boolean.parseBoolean(properties.getProperty("ignoreTrailingWhiteSpace")));

        Dataset<Row> untypedDF = csvReader.csv(getSourcePath());

        StructType preliminarySchema = untypedDF.schema();

        List<String> fieldNames = Arrays.asList(preliminarySchema.fieldNames());

        typedDF = schema != null
                ? csvReader.schema(schema.getStructTypeByName(fieldNames)).csv(getSourcePath())
                : csvReader.csv(getSourcePath());

        if (this.isOverwrite() &&
                (partYear != null || partMonth != null || partWeek != null || partDay != null || partHour != null)) {
            throw new RuntimeException("partitioning is not supported when in overwrite mode");
        }

        if (partYear != null) {
            if (partMonth != null) {
                if (partWeek != null) {
                    if (partDay != null) {
                        if (partHour != null) {
                            final Dataset<Row> partitionedDF = typedDF.withColumn("part_year", functions.lit(partYear))
                                    .withColumn("part_month", functions.lit(partMonth))
                                    .withColumn("part_week", functions.lit(partWeek))
                                    .withColumn("part_day", functions.lit(partDay))
                                    .withColumn("part_hour", functions.lit(partHour));
                            partitionedDF.write()
                                    .mode(SaveMode.Append)
                                    .partitionBy("part_year", "part_month", "part_week", "part_day", "part_hour")
                                    .parquet(getTargetPath());

                        } else {
                            final Dataset<Row> partitionedDF = typedDF.withColumn("part_year", functions.lit(partYear))
                                    .withColumn("part_month", functions.lit(partMonth))
                                    .withColumn("part_week", functions.lit(partWeek))
                                    .withColumn("part_day", functions.lit(partDay));
                            partitionedDF.write()
                                    .mode(SaveMode.Append)
                                    .partitionBy("part_year", "part_month", "part_week", "part_day")
                                    .parquet(getTargetPath());
                        }
                    } else {
                        final Dataset<Row> partitionedDF = typedDF.withColumn("part_year", functions.lit(partYear))
                                .withColumn("part_month", functions.lit(partMonth))
                                .withColumn("part_week", functions.lit(partWeek));
                        partitionedDF.write()
                                .mode(SaveMode.Append)
                                .partitionBy("part_year", "part_month", "part_week")
                                .parquet(getTargetPath());
                    }
                } else {
                    final Dataset<Row> partitionedDF = typedDF.withColumn("part_year", functions.lit(partYear))
                            .withColumn("part_month", functions.lit(partMonth));
                    partitionedDF.write()
                            .mode(SaveMode.Append)
                            .partitionBy("part_year", "part_month")
                            .parquet(getTargetPath());
                }
            } else {
                final Dataset<Row> partitionedDF = typedDF.withColumn("part_year", functions.lit(partYear));
                partitionedDF.write()
                        .mode(SaveMode.Append)
                        .partitionBy("part_year")
                        .parquet(getTargetPath());
            }
        } else {
            final SaveMode saveMode = this.isOverwrite() ? SaveMode.Overwrite : SaveMode.Append;

            final Dataset<Row> partitionedDF = typedDF;
            partitionedDF.write()
                    .mode(saveMode)
                    .parquet(getTargetPath());
        }

    }

}
