package datalake.spark.processors;

import datalake.spark.schema.Schema;
import org.apache.spark.sql.SparkSession;

import java.util.Properties;

public abstract class Processor {

    private final Properties properties;
    private final String sourcePath;
    private final String targetPath;
    private boolean overwrite;
    private Schema schema;

    public Processor(final Properties properties, final String sourcePath, final String targetPath, final boolean overwrite) {

        this.properties = properties;
        this.sourcePath = sourcePath;
        this.targetPath = targetPath;
        this.overwrite = overwrite;
    }

    public Properties getProperties() {

        return properties;
    }

    public String getSourcePath() {

        return sourcePath;
    }

    public String getTargetPath() {

        return targetPath;
    }

    public boolean isOverwrite() {
        return overwrite;
    }

    public void setSchema(Schema schema) {

        this.schema = schema;
    }

    public Schema getSchema() {

        return schema;
    }

    public void process(final SparkSession sparkSession) {

        process(sparkSession, null, null, null, null, null);
    }

    public void process(final SparkSession sparkSession, final String partYear) {

        process(sparkSession, null, null, null, null);
    }

    public void process(final SparkSession sparkSession, final String partYear, final String partMonth) {

        process(sparkSession,partYear, partMonth, null, null, null);
    }

    public void process(final SparkSession sparkSession, final String partYear, final String partMonth, final String partWeek) {

        process(sparkSession,partYear, partMonth, partWeek, null, null);
    }

    public void process(final SparkSession sparkSession, final String partYear, final String partMonth, final String partWeek,
                        final String partDay) {

        process(sparkSession,partYear, partMonth, partWeek, partDay, null);
    }

    public abstract void process(final SparkSession sparkSession, final String partYear, final String partMonth, final String partWeek,
                        final String partDay, final String partHour);
}
