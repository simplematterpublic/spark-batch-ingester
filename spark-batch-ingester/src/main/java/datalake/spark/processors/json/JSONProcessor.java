package datalake.spark.processors.json;

import datalake.spark.processors.Processor;
import org.apache.spark.sql.*;

import java.util.Properties;

public class JSONProcessor extends Processor {

    public JSONProcessor(final Properties properties, final String sourcePath, final String targetPath) {
        super(properties, sourcePath, targetPath, false);
    }

    @Override
    public void process(final SparkSession sparkSession, final String partYear, final String partMonth,
                        final String partWeek, final String partDay, final String partHour) {

        final Properties properties = getProperties();
        //logger.info("CSV properties" + properties);
        //logger.info("CSV source path: " + getSourcePath());

        DataFrameReader jsonReader = sparkSession
                .read()
                .option("primitivesAsString", Boolean.parseBoolean(properties.getProperty("primitivesAsString")))
                .option("multiLine", Boolean.parseBoolean(properties.getProperty("multiLine")));

        Dataset<Row> rawDF = jsonReader.json(getSourcePath());

        final String finalPath;

        if (partYear != null) {
            if (partMonth != null) {
                if (partWeek != null) {
                    if (partDay != null) {
                        if (partHour != null) {
                            finalPath = getTargetPath() +
                                    "/" + "part_year=" + partYear +
                                    "/" + "part_month=" + partMonth +
                                    "/" + "part_week=" + partWeek +
                                    "/" + "part_day=" + partDay +
                                    "/" + "part_hour=" + partHour;
                        } else {
                            finalPath = getTargetPath() +
                                    "/" + "part_year=" + partYear +
                                    "/" + "part_month=" + partMonth +
                                    "/" + "part_week=" + partWeek +
                                    "/" + "part_day=" + partDay;
                        }
                    } else {
                        finalPath = getTargetPath() +
                                "/" + "part_year=" + partYear +
                                "/" + "part_month=" + partMonth +
                                "/" + "part_week=" + partWeek;
                    }
                } else {
                    finalPath = getTargetPath() +
                            "/" + "part_year=" + partYear +
                            "/" + "part_month=" + partMonth;
                }
            } else {
                finalPath = getTargetPath() + "/" + "part_year=" + partYear;
            }
        } else {
            finalPath = getTargetPath();
        }
        rawDF.write().mode(SaveMode.Append).json(finalPath);

    }
}
