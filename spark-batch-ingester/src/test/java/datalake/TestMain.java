package datalake;

public class TestMain {

    public static void main(String[] args) {

        final String[] argvars = new String[10];
        argvars[0] = "-master";
        argvars[1] = "local";
        argvars[2] = "-source";
        argvars[3] = "OPMS_BE_evc_2015_jan_stn_0.txt"; // "OPMS_ecv_dststn_2013_01.csv";
        argvars[4] = "-target";
        argvars[5] = "target_parquet";
        argvars[6] = "-year";
        argvars[7] = "2015";
        argvars[8] = "-month";
        argvars[9] = "01";
        //argvars[10] = "-schema";
        //argvars[11] = "OPMS_evc_dststn_2013_schema.json";
        Main.main(argvars);
    }

}
