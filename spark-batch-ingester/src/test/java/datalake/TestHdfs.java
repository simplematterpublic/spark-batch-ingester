package datalake;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.LocatedFileStatus;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.RemoteIterator;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;

public class TestHdfs {

    @Ignore
    @Test
    public void testHdfsLoclFileSystem() {

        Configuration conf = new Configuration();

        String hdfsuri = "file:///";
        conf.set("fs.defaultFS", hdfsuri);

        // Because of Maven
        conf.set("fs.hdfs.impl", org.apache.hadoop.hdfs.DistributedFileSystem.class.getName());
        conf.set("fs.file.impl", org.apache.hadoop.fs.LocalFileSystem.class.getName());

        // Set HADOOP user
        System.setProperty("HADOOP_USER_NAME", "hdfs");
        System.setProperty("hadoop.home.dir", "/");

        FileSystem fs = null;
        try {
            fs = FileSystem.get(URI.create(hdfsuri), conf);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path p = new Path("data/temp");

        try {
            RemoteIterator<LocatedFileStatus> it = fs.listFiles(p, true);

            while (it.hasNext()) {
                LocatedFileStatus fileStatus = it.next();
                System.out.println(fileStatus.getPath().toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }


    }
}
