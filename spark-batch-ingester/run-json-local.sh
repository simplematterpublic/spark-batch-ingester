java -jar -Dconfig=json.conf target/spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar \
    -master local \
    -source ../../data/testing/json/01/test_data_20150101.json \
    -target playground/master/json/01 \
    -type json \
    -year 2017 \
    -month 04 \
    -week 3 \
    -day 15 \
