java -jar -Dconfig=xml.conf target/spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar \
    -master local \
    -source source/input/sample.xml \
    -target source/output/sample.json \
    -type xml \
    -year 2016
