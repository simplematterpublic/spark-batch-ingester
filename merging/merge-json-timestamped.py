import os
import sys
import json
import subprocess

from merging import utils

YARN = True


def execute_hadoop_ls(source_dir, recursively=False):
    """Execute hadoop fs -ls command as a subprocess."""
    rec_flag = '-R' if recursively else ''
    command = ['hadoop', 'fs', '-ls', rec_flag, source_dir]
    hadoop_ls_proc = subprocess.Popen(
        command, stdout=subprocess.PIPE, universal_newlines=True)

    output = hadoop_ls_proc.stdout

    return output


def create_partitioning_dirs(root_dir,
                             year='', month='', week='', day='', hour=''):
    if not (isinstance(year, str)
            and isinstance(month, str)
            and isinstance(week, str)
            and isinstance(day, str)
            and isinstance(hour, str)):
        raise Exception('partitions should be string')

    parts = {'part_year': year,
             'part_month': month,
             'part_week': week,
             'part_day': day,
             'part_hour': hour}

    final_path = root_dir.rstrip('/')

    for part_label, part_value in parts.items():
        if part_value:
            final_path += '/{}={}'.format(part_label, part_value)
        else:
            break

    command = ['hadoop', 'fs', '-mkdir', '-p', final_path]
    hadoop_mkdir_proc = subprocess.Popen(
        command, stdout=subprocess.PIPE, universal_newlines=True)

    output = hadoop_mkdir_proc.stdout

    return_code = hadoop_mkdir_proc.wait()
    print('return code for hadoop mkdir', return_code)

    return final_path


def copy_to_single_partition(dirs, target_dir):
    """Move contents of directories into a single target dir."""
    command = ['hadoop', 'fs', '-cp']
    for dir in dirs:
        command.append(dir.rstrip('/') + '/*.json')

    command.append(target_dir)

    hadoop_mv_proc = subprocess.Popen(
        command, stdout=subprocess.PIPE, universal_newlines=True)

    output = hadoop_mv_proc.stdout

    return output


def remove_temporary_dir(dir):
    command = ['hadoop', 'fs', '-rm', '-r', '-f']

    if '_output' not in dir and 'part_' not in dir:
        return

    command.append(dir)

    hadoop_rm_rf_proc = subprocess.Popen(
        command, stdout=subprocess.PIPE, universal_newlines=True)

    output = hadoop_rm_rf_proc.stdout

    return output


def is_current(ts, year='', month='', week='', day='', hour=''):
    if not (isinstance(year, str)
            and isinstance(month, str)
            and isinstance(week, str)
            and isinstance(day, str)
            and isinstance(hour, str)):
        raise Exception('partitions should be string')

    if year and month and week and day and hour:
        return (ts.year == int(year)
                and ts.month == int(month)
                and ts.week == int(week)
                and ts.day == int(day)
                and ts.hour == int(hour))
    elif year and month and week and day:
        return (ts.year == int(year)
                and ts.month == int(month)
                and ts.week == int(week)
                and ts.day == int(day))
    elif year and month and week:
        return (ts.year == int(year)
                and ts.month == int(month)
                and ts.week == int(week))
    elif year and month:
        return ts.year == int(year) and ts.month == int(month)
    elif year:
        return ts.year == int(year)
    else:
        raise Exception('No partitioning parameter provided.')


def execute_job(name, source_file, target_path, hdfs_mode):
    print('source file:', source_file)
    print('target path:', target_path)
    print('hdfs_mode:', hdfs_mode)

    # partitioning args example: ['-year', '2015', '-month', '01']
    partitioning_args = utils.extract_partitioning_args_from_path(source_file)

    if hdfs_mode:
        args = [
            '/opt/mapr/spark/spark-2.2.1/bin/spark-submit',
            '--name', 'merge' + '-' + name,
            '--master', 'yarn' if YARN else 'local',
            '--deploy-mode', 'cluster' if YARN else 'client',
            '--files' if YARN else '', 'merge.conf#merge.conf' if YARN else '',
            '--driver-java-options', '-Dconfig=merge.conf',
            # TODO: discover jar file under spark-job folder instead of hardcoding
            'spark-job/spark-json-merger-1.0-SNAPSHOT-jar-with-dependencies.jar'
        ] + [
            '-master', 'cluster',
            '-source', source_file,
            '-target', target_path,
            # '-numFiles', '4',
        ] + partitioning_args
    else:
        raise Exception('Local file system is not supported yet.')
        # args = [
        #     'java', '-jar', '-Dconfig=merge.conf',
        #     'spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar'
        # ] + [
        #     '-master', 'local',
        #     '-source', source_file,
        #     '-target', target_path,
        # ] + partitioning_args

    print('executing command:', ' '.join(args))
    proc = subprocess.Popen(args,
                            stderr=subprocess.PIPE,
                            universal_newlines=True)
    output = proc.communicate()
    with open('stderr/' + os.path.basename(source_file), 'w') as proc_output:
        proc_output.write(output[1])

    return proc.returncode


def checkpoint_diff(checkpoint_path, file_list):
    # read checkpoint file and diff names
    with open(checkpoint_path, 'r') as fh:
        checkpointed_files = [line.strip() for line in fh]
        print('checkpointed_files:')
        print(checkpointed_files)
    # emit pseudo-command for each non-processed file
    unprocessed_files = [file for file in file_list
                         if file not in checkpointed_files]
    print('unprocessed_files:')
    print(unprocessed_files)

    return unprocessed_files


def run_ingestion(name,
                  source_dir,
                  target_dir,
                  checkpoint_path):
    hdfs_mode = (source_dir.startswith('maprfs://')
                 or source_dir.startswith('hdfs://'))

    # list content under hadoop source dir
    output = execute_hadoop_ls(source_dir, recursively=True)

    # detect UUID_TIMESTAMP directories
    dirs = utils.get_timestamped_dirs(output)
    # use partitioning information to filter timestamps
    # TODO: proper convention needed here
    root_dir = source_dir.rstrip('/') + '_output'
    # Where do these partitions come from
    year, month = '2015', '01'

    timestamps = [utils.try_extract_timestamp(name) for name in dirs]

    filtered_dirs = [name for name, ts in zip(dirs, timestamps)
                     if ts is not None and is_current(ts, year, month)]

    # take first unprocessed directory
    if filtered_dirs:
        # create partitioning dirs accordingly
        new_partition_dir = create_partitioning_dirs(root_dir, year, month)
        # consolidate files to merge into a single partition folder
        copy_to_single_partition(filtered_dirs, new_partition_dir)
        # write the result into the given partitioning path
        source = new_partition_dir
    # Or just return if there is no unprocessed directories
    else:
        print("No partitions to be merged.")
        return

    # TODO: How do we checkpoint this?
    # # filter against checkpoint
    # unprocessed_dirs = checkpoint_diff(checkpoint_path, dir_paths)

    return_code = execute_job(name=name,
                              source_file=source,
                              target_path=target_dir,
                              hdfs_mode=hdfs_mode)

    if return_code == 0:
        with open(checkpoint_path, 'a') as updated_checkpoint_file:
            updated_checkpoint_file.readline
            updated_checkpoint_file.write(source)
            updated_checkpoint_file.write('\n')
        print('Processed', source)

        remove_temporary_dir(new_partition_dir)
    else:
        print('Failed', source)


if __name__ == '__main__':
    if len(sys.argv) == 1:
        raise Exception("configuration file path is expected as argument")
    elif len(sys.argv) == 2:
        config_path = sys.argv[1]
    else:
        raise Exception("too many arguments passed")

    print('args received:', sys.argv)

    with open(config_path, 'r') as config_file:
        config = json.load(config_file)

    print('config:', config)

    name = config['name']

    for item in config['data']:
        source_dir = item['source']
        target_dir = item['target']
        checkpoint_path = item['checkpoint_path']
        run_ingestion(name,
                      source_dir,
                      target_dir,
                      checkpoint_path)
