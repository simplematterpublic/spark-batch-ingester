from merging import utils


def test_parts():
    with open('data/hadoop_example_output.txt') as file:
        hadoop_output = [line for line in file]

    dir_paths = utils.get_fully_partitioned_dirs(hadoop_output)

    args = utils.extract_partitioning_args_from_path(dir_paths[0])

    print(dir_paths)
    print(dir_paths[0])
    print(args)
