# JSON Merging

To run the merging script use `run-merge.sh` script. It executes python script `merge-json.py` with default configuration file.

`merge-json.py` script launches spark job packaged in jar file, located under `spark-job` directory, which processes one partition on a single run in overwrite mode. This means that the target directory (normally master) will contain fully merged fresh copy of the data without duplication. Additionally `merge-json.py` has simple checkpointing mechanism where it notes already processed partitions. On each run it will take first item of unprocessed partitions from source.

Note that if merging was performed on the source which is incomplete, i.e. some data will be added to it later, to update the master to include all data, it is necessary to remove the record from checkpoint and re-run the script for the time being, later this scenario can be supported by the `merge-json.py` script for convenience.
