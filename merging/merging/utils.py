import uuid
import datetime


def get_timestamped_dirs(hadoop_output):
    """Get directories that follow UUID_TIMESTAMP naming convention."""
    output_lines = [line.strip() for line in hadoop_output
                    if len(line.strip())]
    output_matrix = [line.split(" ") for line in output_lines]
    dir_list = [row[-1].strip() for row in output_matrix
                if len(row) > 6 and row[0].startswith('d')]

    timestamped_dirs = [name for name in dir_list if is_valid(name)]

    print(dir_list)
    print(timestamped_dirs)

    return timestamped_dirs


def get_fully_partitioned_dirs(hadoop_output):
    """Get list of directory paths that contain full partitioning.

    By full partitioning is meant that under this path there are
    already files located.
    """
    output_lines = [line.strip() for line in hadoop_output
                    if len(line.strip())]
    output_matrix = [line.split(" ") for line in output_lines]
    dir_list = [row[-1].strip() for row in output_matrix
                if len(row) > 6 and row[0].startswith('d')]

    partitions = [
        [dir] + [entry for entry in dir.split('/')
                 if (entry.startswith('part_year=')
                     or entry.startswith('part_month=')
                     or entry.startswith('part_week=')
                     or entry.startswith('part_day=')
                     or entry.startswith('part_hour='))]
        for dir in dir_list]

    max_length = max(len(part) for part in partitions)

    fully_partitioned_dirs = [part[0]
                              for part in partitions
                              if len(part) == max_length]

    return fully_partitioned_dirs


def extract_partitioning_args_from_path(dir_path):
    """Extract list of partitioning arguments from directory.

    Example:
        from path: '/some/path/to/data/part_year=2015/part_month=01'
        extract arg list: ['-year', '2015', '-month', '01']
    """
    partition_dict = {entry.split('=')[0][5:]: entry.split('=')[1]
                      for entry in dir_path.split('/')
                      if (entry.startswith('part_year=')
                          or entry.startswith('part_month=')
                          or entry.startswith('part_week=')
                          or entry.startswith('part_day=')
                          or entry.startswith('part_hour='))}

    partition_nested_list = [['-' + key, val]
                             for key, val in partition_dict.items()]

    partition_flat_list = [item
                           for sublist in partition_nested_list
                           for item in sublist]

    return partition_flat_list


def extract_timestamp(name):
    is_string = isinstance(name, str)

    dir_name = name.split('/')[-1] if '/' in name else name

    if not is_string:
        raise Exception("Name is not string")

    if '_' not in dir_name:
        raise Exception("Name does not contain '_' delimiter")

    split = dir_name.split('_')

    if len(split) != 2:
        raise Exception(
            "Name does not split into two parts with '_' delimiter")

    id_str, ts_str = split

    try:
        uuid.UUID(id_str)
    except Exception as ex:
        raise Exception("UUID {} is not valid".format(id_str))

    try:
        ts = int(ts_str) / 1000
        dt = datetime.datetime.fromtimestamp(ts)
        # If ts is on different scale, validation fails
        if dt.year == 1970:
            raise Exception("Timestamp {} is not valid or does not have "
                            "millisecond scaling".format(ts_str))
    except Exception as ex:
        raise Exception("Timestamp {} is not valid".format(ts_str))

    return dt


def try_extract_timestamp(name):
    try:
        return extract_timestamp(name)
    except Exception as ex:
        print(ex)
        return None


def is_valid(name):
    try:
        extract_timestamp(name)
    except Exception as ex:
        return False

    return True
