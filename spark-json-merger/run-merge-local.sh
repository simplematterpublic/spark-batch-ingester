java -jar -Dconfig=json.conf target/spark-batch-ingester-1.0-SNAPSHOT-jar-with-dependencies.jar \
    -master local \
    -source data/input/json/part_year=2016 \
    -target data/output/json/part_year=2016 \
    -type merge
