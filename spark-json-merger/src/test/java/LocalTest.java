import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.hdfs.DistributedFileSystem;
import org.junit.Ignore;
import org.junit.Test;

import java.io.IOException;
import java.net.URI;

public class LocalTest {

    @Ignore
    @Test
    public void testHdfs() {

// ====== Init HDFS File System Object
        Configuration conf = new Configuration();
// Set FileSystem URI
        String hdfsuri = "file:///";
        conf.set("fs.defaultFS", hdfsuri);
// Because of Maven
        conf.set("fs.hdfs.impl", DistributedFileSystem.class.getName());
        conf.set("fs.file.impl", LocalFileSystem.class.getName());
// Set HADOOP user
        System.setProperty("HADOOP_USER_NAME", "hdfs");
        System.setProperty("hadoop.home.dir", "/");
//Get the filesystem - HDFS
        FileSystem fs = null;
        try {
            fs = FileSystem.get(URI.create(hdfsuri), conf);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path p = new Path("data/temp");

        final RemoteIterator<LocatedFileStatus> statusRemoteIterator;
        try {
            statusRemoteIterator = fs.listFiles(p, true);

            while (statusRemoteIterator.hasNext()) {
                final LocatedFileStatus fileStatus = statusRemoteIterator.next();
                System.out.println(fileStatus.getPath().toString());
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
