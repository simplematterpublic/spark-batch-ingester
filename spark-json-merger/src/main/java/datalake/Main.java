package datalake;

import datalake.spark.json.JSONMerger;
import org.apache.commons.cli.*;
import org.apache.spark.SparkConf;
import org.apache.spark.SparkContext;
import org.apache.spark.sql.SparkSession;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Main {

    public static void main(String[] args) {

        final Options options = new Options();

        final Option modeOption = new Option("master", "master", true, "Master (cluster or local)");
        modeOption.setRequired(true);
        options.addOption(modeOption);

        final Option sourceOption = new Option("source", "source", true, "Source path");
        sourceOption.setRequired(true);
        options.addOption(sourceOption);

        final Option targetOption = new Option("target", "target", true, "Target path");
        targetOption.setRequired(true);
        options.addOption(targetOption);

        final Option numFilesOption = new Option("numFiles", "partitions", true, "Maximum number of files in each partition");
        numFilesOption.setRequired(false);
        options.addOption(numFilesOption);

        final Option partYearOption = new Option("year", "year", true, "Part Year");
        partYearOption.setRequired(false);
        options.addOption(partYearOption);

        final Option partMonthOption = new Option("month", "month", true, "Part Month");
        partMonthOption.setRequired(false);
        options.addOption(partMonthOption);

        final Option partWeekOption = new Option("week", "week", true, "Part Week");
        partWeekOption.setRequired(false);
        options.addOption(partWeekOption);

        final Option partDayOption = new Option("day", "day", true, "Part Day");
        partDayOption.setRequired(false);
        options.addOption(partDayOption);

        final Option partHourOption = new Option("hour", "hour", true, "Part Hour");
        partHourOption.setRequired(false);
        options.addOption(partHourOption);

        final CommandLineParser parser = new BasicParser();
        final HelpFormatter formatter = new HelpFormatter();

        try {
            final CommandLine commandLine = parser.parse(options, args);
            final String master = commandLine.getOptionValue("master");
            final String source = commandLine.getOptionValue("source");
            final String target = commandLine.getOptionValue("target");
            final String numFiles = commandLine.getOptionValue("numFiles");
            final String partYear = commandLine.getOptionValue("year");
            final String partMonth = commandLine.getOptionValue("month");
            final String partWeek = commandLine.getOptionValue("week");
            final String partDay = commandLine.getOptionValue("day");
            final String partHour = commandLine.getOptionValue("hour");
            final Properties properties = loadConfig(System.getProperty("config"));

            final SparkSession sparkSession;

            if ("cluster".toLowerCase().equals(master.trim().toLowerCase())) {
                sparkSession = SparkSession.builder().getOrCreate();
            } else {
                final SparkConf sparkConf = new SparkConf().setAppName("local").setMaster("local");
                final SparkContext sparkContext = new SparkContext(sparkConf);
                sparkSession = new SparkSession(sparkContext).newSession();
            }

            final JSONMerger jsonMerger = new JSONMerger(properties, source.trim(), target.trim());

            jsonMerger.process(sparkSession, Integer.parseInt(numFiles == null ? "1" : numFiles),
                    partYear == null ? null : partYear.trim(), partMonth == null ? null : partMonth.trim(),
                    partWeek == null ? null : partWeek.trim(), partDay == null  ? null : partDay.trim(),
                    partHour == null ? null : partHour.trim());

            sparkSession.close();

        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("spark-json-merger", options);
            System.exit(1);
        }
    }

    private static Properties loadConfig(final String config) {

        final Properties props = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream(config);
            props.load(input);
        } catch (IOException ex) {
            ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return props;
    }

}
