package datalake.spark.json;

import org.apache.spark.sql.*;
import java.util.Properties;

public class JSONMerger {

    private final Properties properties;
    private final String sourcePath;
    private final String targetPath;

    public JSONMerger(final Properties properties, final String sourcePath, final String targetPath) {

        this.properties = properties;
        this.sourcePath = sourcePath;
        this.targetPath = targetPath;
    }

    public Properties getProperties() {
        return properties;
    }

    public String getSourcePath() {
        return sourcePath;
    }

    public String getTargetPath() {
        return targetPath;
    }

    public void process(final SparkSession sparkSession, final int numPartitions) {

        process(sparkSession, numPartitions, null, null, null, null, null);
    }

    public void process(final SparkSession sparkSession, final int numPartitions, final String partYear) {

        process(sparkSession, numPartitions, null, null, null, null);
    }

    public void process(final SparkSession sparkSession, final int numPartitions, final String partYear,
                        final String partMonth) {

        process(sparkSession, numPartitions, partYear, partMonth, null, null, null);
    }

    public void process(final SparkSession sparkSession, final int numPartitions, final String partYear,
                        final String partMonth, final String partWeek) {

        process(sparkSession, numPartitions, partYear, partMonth, partWeek, null, null);
    }

    public void process(final SparkSession sparkSession, final int numPartitions, final String partYear,
                        final String partMonth, final String partWeek, final String partDay) {

        process(sparkSession, numPartitions, partYear, partMonth, partWeek, partDay, null);
    }

    public void process(final SparkSession sparkSession, final int numPartitions, final String partYear,
                        final String partMonth, final String partWeek, final String partDay, final String partHour) {

        final Properties properties = getProperties();

        DataFrameReader jsonReader = sparkSession
                .read()
                .option("primitivesAsString", Boolean.parseBoolean(properties.getProperty("primitivesAsString")))
                .option("multiLine", Boolean.parseBoolean(properties.getProperty("multiLine")));

        Dataset<Row> beforeMergingDF = jsonReader.json(getSourcePath());

        Dataset<Row> mergedDF = beforeMergingDF.coalesce(numPartitions);

        final String finalPath;

        if (partYear != null) {
            if (partMonth != null) {
                if (partWeek != null) {
                    if (partDay != null) {
                        if (partHour != null) {
                            finalPath = getTargetPath() +
                                    "/" + "part_year=" + partYear +
                                    "/" + "part_month=" + partMonth +
                                    "/" + "part_week=" + partWeek +
                                    "/" + "part_day=" + partDay +
                                    "/" + "part_hour=" + partHour;
                        } else {
                            finalPath = getTargetPath() +
                                    "/" + "part_year=" + partYear +
                                    "/" + "part_month=" + partMonth +
                                    "/" + "part_week=" + partWeek +
                                    "/" + "part_day=" + partDay;
                        }
                    } else {
                        finalPath = getTargetPath() +
                                "/" + "part_year=" + partYear +
                                "/" + "part_month=" + partMonth +
                                "/" + "part_week=" + partWeek;
                    }
                } else {
                    finalPath = getTargetPath() +
                            "/" + "part_year=" + partYear +
                            "/" + "part_month=" + partMonth;
                }
            } else {
                finalPath = getTargetPath() + "/" + "part_year=" + partYear;
            }
        } else {
            finalPath = getTargetPath();
        }

        mergedDF.write().mode(SaveMode.Overwrite).json(finalPath);
    }

}
